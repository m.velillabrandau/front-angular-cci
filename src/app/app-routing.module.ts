import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './components/user-list/user-list.component';
import { CatalogueListComponent } from './components/catalogue-list/catalogue-list.component';
import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './nav/nav.component';
import { MainCatalogueComponent } from './components/main-catalogue/main-catalogue.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { IconographyListComponent } from './components/iconography-list/iconography-list.component';
import { LogoutComponent } from './components/logout/logout.component';
// rutas de paginas
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'login/:end', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'logout/:end', component: LogoutComponent },
  { path: '', component: NavComponent,
    children: [
      { path: 'user-list', component: UserListComponent },
      { path: 'catalogue-list', component: CatalogueListComponent },
      { path: 'editor/:id', component: MainCatalogueComponent },
      { path: 'iconography', component: IconographyListComponent }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
