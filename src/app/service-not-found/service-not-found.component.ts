import { Component } from '@angular/core';

@Component({
  selector: 'app-service-not-found',
  templateUrl: './service-not-found.component.html',
  styleUrls: ['./service-not-found.component.css']
})
export class ServiceNotFoundComponent {
  constructor() { }
}
