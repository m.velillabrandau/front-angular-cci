import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { CatalogueService } from '../../../services/catalogue.service';
import { FeriaService } from '../../../services/feria.service';
import { DialogComponent } from "../../shared/dialog/dialog.component";

export interface Ferias {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-catalog',
  templateUrl: './edit-catalog.component.html',
  styleUrls: ['./edit-catalog.component.css']
})
export class EditCatalogComponent implements OnInit {
  catalogEditForm: FormGroup;
  ferias: Ferias[] = [];
  data: Array<any> = [];
  selected: number = 0;

  constructor( public dialog: DialogComponent, public dialogRef: MatDialog, private catService: CatalogueService, private feriaService: FeriaService) { }

  ngOnInit() {
    this.data = this.dialog.getData();
    this.getferias();
    this.catalogEditForm = new FormGroup({
      'title': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'feria': new FormControl(null,[
        Validators.required,
      ]),
    });

    this.selected = this.data['data']['id_fair'];



  }

  // Cierra popup de ingreso
  onNoClick(): void {
    this.dialogRef.closeAll();
  }

  //Carga las ferias en la interface
  getferias() {
    this.feriaService.getFerias().subscribe((data: {}) => {

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          this.ferias.push({value: key, viewValue: data[key]})
        }
      }
    });
  }

  // Reinicio componentes
  onSubmit() {
    if (this.catalogEditForm.valid) {
      this.catService.editCatalog({"id": this.data['data']['id'], "feria_alias":this.catalogEditForm.value.title, 'ferias_id': this.catalogEditForm.value.feria}).then(response => {
        //Success
      }).catch( error => {
        //fail
      });
      this.dialogRef.closeAll();
      // this.dialogRef.closeAll();
    }
  }

  get title() { return this.catalogEditForm.get('title'); }
  get feria() { return this.catalogEditForm.get('feria'); }
  matcher = new ErrorStateMatcher();

}
