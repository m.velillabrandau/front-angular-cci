import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { AddCatalogComponent } from './add-catalog/add-catalog.component';
import { EditCatalogComponent } from './edit-catalog/edit-catalog.component';
import { DeleteCatalogueComponent } from './delete-catalogue/delete-catalogue.component';
import { CatalogueService } from '../../services/catalogue.service';
import { SwalComponent } from '../shared/swal/swal.component';
import { environment } from '../../../environments/environment';
const PDFEndpoint = environment.UrlPdf; // ruta pdf

export interface FeriaData {
  id: Number;
  active: Number;
	ferias_id: string;
	feria_alias: string;
  created: string;
  title: string;
}

@Component({
	selector: 'app-catalogue-list',
	templateUrl: './catalogue-list.component.html',
	styleUrls: ['./catalogue-list.component.css']
})
export class CatalogueListComponent implements OnInit {

	displayedColumns: string[] = ['id', 'ferias_id', 'title', 'feria_alias', 'created', 'acciones'];
  dataSource: MatTableDataSource<FeriaData>;
  dataCatalog = {
    id: null,
    title: null,
    id_fair: null
  };


	@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

	constructor(public dialog: MatDialog, private catalogueService: CatalogueService) { }
  // Iniciación de clase
	ngOnInit() {
    this.getCatalogs();
    this.catalogueService.change.subscribe(change => {
      this.getCatalogs();
    });
  }
  //Obtiene todos los catálogos
  public getCatalogs() {
    this.catalogueService.getCatalogues().subscribe((data: {}) => {
      this.dataSource = new MatTableDataSource(data as Array<FeriaData>);
      //Configuraciones paginador y ordenado
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
      this.paginator._intl.nextPageLabel = 'Página siguiente';
      this.paginator._intl.previousPageLabel = 'Página anterior';
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  getCatalog(id){
    this.catalogueService.getCataloge(id).subscribe(
			response=>{
        this.dataCatalog.id = id;
        this.dataCatalog.title = response.feria_alias;
        this.dataCatalog.id_fair= response.ferias_id;
				if(response!=''&&response!=undefined){
          const dialogRef = this.dialog.open(DialogComponent, {
            width: '300px',
            data: { component: EditCatalogComponent, title: "Editar Catálogo", data: this.dataCatalog }
          });

          // return response;

				}else{
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido obtener el Catálogo'
          });
					// return response;
				}
			},error=>{
				console.log(error);
				// return false;
			}
		);

  }
  // Filtro de busqueda, este filtra por cualquier campo de la grilla
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
  }
  // Abre popup de agregar catálogo
  openModalCatalog(): void{
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: { component: AddCatalogComponent, title: "Agregar Catálogo" }
    });
  }

  //Abre Modal para editar un catalogo
  openModalEditCatalog(id): void{
    this.getCatalog(id);
  }

  // Abre Modal para eliminar catalogo
  openModalDeleteCatalog(id): void{

    const dialogRef = this.dialog.open(DialogComponent, {
      width: '340px',
      data: { component: DeleteCatalogueComponent, title: "Eliminar Catálogo", id: id }
    });
  }
  exportPDF(id) {
    window.open(PDFEndpoint + id + '.pdf', "_blank");
  }
}
