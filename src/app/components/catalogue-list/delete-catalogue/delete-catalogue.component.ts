import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from "../../shared/dialog/dialog.component";
import { CatalogueService } from '../../../services/catalogue.service';
import { SwalComponent } from '../../shared/swal/swal.component';
@Component({
  selector: 'app-delete-catalogue',
  templateUrl: './delete-catalogue.component.html',
  styleUrls: ['./delete-catalogue.component.css']
})
export class DeleteCatalogueComponent implements OnInit {
  data: Array<any> = [];
  constructor(public dialogRef: MatDialog, public dialog: DialogComponent, public catServ: CatalogueService) { }

  ngOnInit() {
    this.data = this.dialog.getData();
  }

  onNoClick(): void {
    this.dialogRef.closeAll();
  }
  deleteCatalog() {
    this.catServ.deleteCatalog(this.data["id"]);
    this.dialogRef.closeAll();
  }

}
