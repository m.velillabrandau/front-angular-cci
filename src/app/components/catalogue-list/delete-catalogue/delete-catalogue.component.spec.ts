import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCatalogueComponent } from './delete-catalogue.component';

describe('DeleteCatalogueComponent', () => {
  let component: DeleteCatalogueComponent;
  let fixture: ComponentFixture<DeleteCatalogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteCatalogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
