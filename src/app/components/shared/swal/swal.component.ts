import { Component } from '@angular/core';
import swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-swal',
  templateUrl: './swal.component.html',
})

// Configuración de mensajes de validación
export class SwalComponent {
  public static toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
}
