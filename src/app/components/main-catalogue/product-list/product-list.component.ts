import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MediatorService } from '../../../services/mediator.service';
import { ProductService } from '../../../services/product.service';
import { SwalComponent } from '../../../components/shared/swal/swal.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit, OnDestroy {
  @Input() products : Array<any> = [];
  @Input() pageId: number = null;
  @Input() tags : Array<any> = [];
  @Input() gaName: string = "";
  //** Filtros **/
  filtname: boolean = true;
  filtmadre: boolean = true;
  filtdiseno: boolean = true;
  filtn3: boolean = true;
  filtn4: boolean = true;
  filtpp: boolean = true;
  @Input() noProducts: boolean = false;
  dsubs: Subscription;
  //************/
  constructor(private mediator: MediatorService, public productService: ProductService) { }
  ngOnInit() {
    this.dsubs = this.mediator.addProduct.subscribe(prod => {
      this.products.push(prod);
      this.noProducts = false;
    });

  }
  ngOnDestroy(){
    this.dsubs.unsubscribe();
  }
  // Cuando se desea filtrar por algún criterio, gatillado desde el select
  selectFilter(event) {
    switch(event) {
      case 1: {
        if(this.filtname) {
          this.products.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
          this.filtname = false;
        } else {
          this.products.sort((a,b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0));
          this.filtname = true;
        }
        break;
      }
      case 2: {
        if(this.filtmadre) {
          this.products.sort((a,b) => (a.madre_id > b.madre_id) ? 1 : ((b.madre_id > a.madre_id) ? -1 : 0));
          this.filtmadre = false;
        } else {
          this.products.sort((a,b) => (a.madre_id < b.madre_id) ? 1 : ((b.madre_id < a.madre_id) ? -1 : 0));
          this.filtmadre = true;
        }
        break;
      }
      case 3: {
        if(this.filtdiseno) {
          this.products.sort((a,b) => (a.diseno > b.diseno) ? 1 : ((b.diseno > a.diseno) ? -1 : 0));
          this.filtdiseno = false;
        } else {
          this.products.sort((a,b) => (a.diseno < b.diseno) ? 1 : ((b.diseno < a.diseno) ? -1 : 0));
          this.filtdiseno = true;
        }
        break;
      }
      case 4: {
        if(this.filtn3) {
          this.products.sort((a,b) => (a.codn3 > b.codn3) ? 1 : ((b.codn3 > a.codn3) ? -1 : 0));
          this.filtn3 = false;
        } else {
          this.products.sort((a,b) => (a.codn3 < b.codn3) ? 1 : ((b.codn3 < a.codn3) ? -1 : 0));
          this.filtn3 = true;
        }
        break;
      }

      case 5: {
        if(this.filtn4) {
          this.products.sort((a,b) => (a.codn4 > b.codn4) ? 1 : ((b.codn4 > a.codn4) ? -1 : 0));
          this.filtn4 = false;
        } else {
          this.products.sort((a,b) => (a.codn4 < b.codn4) ? 1 : ((b.codn4 < a.codn4) ? -1 : 0));
          this.filtn4 = true;
        }
        break;
      }
      case 6: {
        if(this.filtpp) {
          this.products.sort((a,b) => (a.punta_precio > b.punta_precio) ? 1 : ((b.punta_precio > a.punta_precio) ? -1 : 0));
          this.filtpp = false;
        } else {
          this.products.sort((a,b) => (a.punta_precio < b.punta_precio) ? 1 : ((b.punta_precio < a.punta_precio) ? -1 : 0));
          this.filtpp = true;
        }
        break;
      }

    }
  }
}
