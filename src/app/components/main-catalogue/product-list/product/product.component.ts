import { Component, Input, OnInit } from '@angular/core';
import { SwalComponent } from '../../../../components/shared/swal/swal.component';
import { IconographyService } from '../../../../services/iconography.service';
import { ProductService } from '../../../../services/product.service';
import { MediatorService } from '../../../../services/mediator.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from './../../../../../environments/environment';
import { MainCatalogueComponent } from './../../main-catalogue.component';
import { OptionalTextComponent } from './../../main-grid-products/grid/square-grid/pdf-creation/optional-text/optional-text.component'
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { ActivatedRoute } from '@angular/router';

const APIEndpoint = environment.URLImg; //rute api

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Input() pageId: number = null;
  @Input() contentId: number = 0;
  @Input() exist: number = 0;
  @Input() product : any = [];
  @Input() class : string = 'normal';
  @Input() moreSku : Array<any> = [];
  @Input() tags : Array<any> = [];
  @Input() setState : Array<any> = [];
  markTag: string ="";
  markLess: string ="";
  markMore: string ="";
  markNew: string ="";
  markSku: string ="";
  routeImg: string ="";
  optionalText: string ="";
  textAreaAddForm: FormGroup;
  idcat: number = null;

  ruteTags = environment.URLTags; //rute tags

  constructor(private mediator: MediatorService, private activeRouter: ActivatedRoute, public dialog: MatDialog, private main: MainCatalogueComponent, private iconographyService: IconographyService, private productService: ProductService) { }
  ngOnInit(){

   

    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
    });

    let skumod = String(this.product['sku']).substring(5);
    this.routeImg = APIEndpoint + skumod + '.jpg';
    //this.getIconographies();
    this.setValueProduct();
    this.textAreaAddForm = new FormGroup({
      'optional_text': new FormControl(null,[
      ])
    })
    this.optionalText = this.product['measurements'];
  }

  //Guarda texto opcional
  saveOptional(sku, hijo, isnew, ppVal, measurement) {
    this.openModalTextOptional(sku, hijo, isnew, ppVal, measurement);
  }
  //abre el modal de texto opcional
  openModalTextOptional(sku, hijo, isnew, ppVal, measurement): void {
    let text = "";
    if(hijo) {
      text = measurement;
    } else {
      text = this.product['measurements'];
    }
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {component: OptionalTextComponent, title: "Opciones Producto", sku: sku, pageId: this.pageId, hijo: hijo, product: this.product, isnew: isnew, ppVal: ppVal, optionalText: text, idcat: this.idcat }
    });
    dialogRef.afterClosed().subscribe(result => {

      if(result[0] == "success") {
        if(this.product['sku'] == sku) {
          this.product['measurements'] = result[1];
        }
        for(var i in this.moreSku) {
          if(this.moreSku[i][0] == sku){
            this.moreSku[i][1] = result[1];
            this.moreSku[i][2] = result[2]['ppVal'];
            this.moreSku[i][3] = result[2]['isnew'];
            this.moreSku[i][4] = result[1];

          }
        }
      } else if(result[0] == "closed") {
        console.log("RESULTADO: ",result);
        if(this.product['sku'] == sku){
          this.product['is_new'] = result[2]['isnew'];
        }
        for(var i in this.moreSku) {
          if(this.moreSku[i][0] == sku){
            this.moreSku[i][2] = result[2]['ppVal'];
            this.moreSku[i][3] = result[2]['isnew'];
            if(result[2]['isnew'] == 1) {
              let elem = document.getElementById('sku-children-'+sku);
              elem.setAttribute('style', "");
              elem.setAttribute('style', "cursor: pointer; margin: 1px;background-color: yellow");
            } else {
              let elem = document.getElementById('sku-children-'+sku);
              elem.setAttribute('style', "");
              elem.setAttribute('style', "cursor: pointer; margin: 1px;");
            }
          }
        }
      }
    });
  }
  //Evento se seleccionar tag
  selectTag(tagId) {
    let data = {
      tagId: tagId,
      pageId: this.pageId,
      id_cat: this.idcat
    }
    this.productService.saveIconographyProduct(this.product['sku'], data).then(res=> {
      if((res['tags_id'] != 0)&&(res['tags_id'] != null)&&(res['tags_id'] != undefined)){
        this.markTag = "green";
      }else{
        this.markTag = "black";
      }
    });

  }

  giveValue(typeValue){
    let data = {
      typeValue: typeValue,
      pageId: this.pageId,
      id_cat: this.idcat
    }
    this.productService.setValueProduct(this.product['sku'], data).then(res=> {

      if(res['pp_symbol'] == 1 ){
        this.markMore = "green";
        this.markLess = "black";
      }

      if(res['pp_symbol'] == 2 ){
        this.markLess = "green";
        this.markMore = "black";
      }

      if(res['pp_symbol'] == 0 ){
        this.markMore = "black";
        this.markLess = "black";
      }

      if ( (res['is_new'] == 1 )  ){
        this.markNew = "green";
        this.markSku = "yellow";
      }else{
        this.markNew = "black";
        this.markSku = "white";
      }
    });
  }

  setValueProduct(){
    if ( (this.product['tags_id'] != 0 )&& (this.product['tags_id'] != undefined) ){
      this.markTag = "green";
    }

    if(this.product['pp_symbol'] == 1 ){
      this.markMore = "green";
    }

    if(this.product['pp_symbol'] == 2 ){
      this.markLess = "green";
    }

    if ( (this.product['is_new'] != 0 )&& (this.product['is_new'] != undefined) && (this.product['is_new'] != null) ){
      this.markNew = "green";
      this.markSku = "yellow";
    }
  }

  addTextarea(){
    if (this.textAreaAddForm.valid) {
      let data = {
        optionalText: this.textAreaAddForm.value.optional_text,
        pageId: this.pageId,
        id_cat: this.idcat
      }
      this.productService.saveOptionalTextProduct(this.product['sku'], data).then(res=> {

      });
    }
  }

  deleteContent(id) {
    this.productService.removeContent(id).then(res => {
      //Se regresan los productos a la lista
      for(var i in res) {
        this.mediator.sendProductToList(res[i]);
      }
      var elem = document.getElementById(this.product['sku']);
      this.exist = 0;
      elem.parentElement.remove();
    }).catch(err=>{})
  }

  giveValueAll(typeValue){

    let data = {
      typeValue: typeValue,
      id_cat: this.idcat
    }

    this.productService.setValueAllProduct(this.contentId, data).then(res=> {

      if(res['pp_symbol'] == 1 ){
        this.markMore = "green";
        this.markLess = "black";
      }

      if(res['pp_symbol'] == 2 ){
        this.markLess = "green";
        this.markMore = "black";
      }

      if(res['pp_symbol'] == 0 ){
        this.markMore = "black";
        this.markLess = "black";
      }

      if ( (res['is_new'] == 1 )  ){
        this.markNew = "green";
        this.product['is_new'] = 1;

        for (let son of this.moreSku) {
          son[3]=1;
        }
        
      }else{
        this.markNew = "black";
        this.product['is_new'] = 0;
        
        for (let son of this.moreSku) {
          son[3]=0;
        }
      }
    });

  }

}
