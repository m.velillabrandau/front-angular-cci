import { Component, OnInit } from '@angular/core';
import { MediatorService } from '../../services/mediator.service';
import { ActivatedRoute } from '@angular/router';
import { IconographyService } from '../../services/iconography.service';
import { ProductService } from '../../services/product.service';
import { GaService } from '../../services/ga.service';
// import product from 'ramda/es/product';


@Component({
  selector: 'app-main-catalogue',
  templateUrl: './main-catalogue.component.html',
  styleUrls: ['./main-catalogue.component.css']
})

export class MainCatalogueComponent implements OnInit {
  sidenavState: any;
  showSidenav: Boolean = false;
  idCat: number;
  show: Boolean = false;
  products: Array<any> = [];
  productsInList: Array<any> = [];
  tags: Array<any> = [];
  contentsGroups: Array<any> = [];
  idLocalGa: number=null;
  idPage: number = null;
  idWorld: number = null;
  worldName:string = null;
  gaName:string = null;
  noProducts: boolean = false;

  constructor(private gaService: GaService, public mediator: MediatorService, private iconographyService: IconographyService, private productService: ProductService) { }

  // Crea sidenav
  ngOnInit() {
    this.sidenavState = this.mediator.getSideNavState();

  }
  //retorna el di del catálogo en el cual se está trabajando
  public getIdCat() {
    return this.idCat;
  }
  // Activa Sidenav
  setState(data, xval, yval, idPage, idLocalGa, worldName?, gaName?, idWorld?) {

    this.products = [];
    this.showSidenav = true;
    this.gaName = gaName;
    this.mediator.sendIdPage(idPage);
    this.idPage = idPage;
    this.mediator.sendYcoord(Array(yval).fill(0).map((x,i)=>i));
    this.mediator.sendXcoord(Array(xval).fill(0).map((x,i)=>i));
    this.idLocalGa = idLocalGa;
    this.products = data;

    this.worldName = worldName;
    this.idWorld = idWorld;
    this.getIconographies();
    this.gaService.getContentGroupByPage(idPage).then((data: {}) => {
      this.contentsGroups = data as Array<any>;
    }).catch(err=>{});

    //Este array contiene toda la data necesaria para renderizar y trabajar en el editor
    let arrEditor = [];

    this.productService.getContentsFrompage(idPage).then(localprds => {

      let prodLocal = [];
      let auxSoons = [];
      this.productsInList = data;
      let soons = []; //los hijos quedan acá y después se agregan al padre
      for(var i in localprds) {
        for(var l in this.productsInList) {
          if(this.productsInList[l]['sku'] == localprds[i]['sku']) {
            this.productsInList.splice(+l,1);
          }
        }
        if(localprds[i]['content_parent_id'] > 0 ) {
          // Es hijo
          soons.push(localprds[i]);
        } else {
          // Es padre
          prodLocal[localprds[i]['c_order']] = localprds[i];
          auxSoons[localprds[i]['id']] = [];
        }
      }

      if(this.productsInList.length == 0){
        this.noProducts = true;
      } else {
        this.noProducts = false;
      }
      // Se incorporan los hijos al padre
      let localSoons = [];
      for(var h in soons) {
        if(soons[h]['content_parent_id'] in auxSoons) {
          auxSoons[soons[h]['content_parent_id']][h]=soons[h];
        }
      }
      for(var p in prodLocal) {
        if(prodLocal[p]['id'] in auxSoons) {
          prodLocal[p]['soons'] = auxSoons[prodLocal[p]['id']];
        }
      }
      arrEditor['Ycoords'] = Array(yval).fill(0).map((x,i)=>i);
      arrEditor['Xcoords'] = Array(xval).fill(0).map((x,i)=>i);
      arrEditor['totalCords'] = Array(xval*yval).fill(0).map((x,i)=>i +1);

      for (var key in arrEditor['totalCords']) {
        if (arrEditor['totalCords'].hasOwnProperty(key)) {
          if(typeof prodLocal[+key+1] !== 'undefined') {
            arrEditor['totalCords'][key] = prodLocal[+key+1];
          }else {
            arrEditor['totalCords'][key] = 'empty';
          }
        }
      }
      arrEditor['productsRemote'] = data;
      arrEditor['idPage'] = idPage;
      arrEditor['idLocalGa'] = idLocalGa;
      arrEditor['contentsGroups'] = this.contentsGroups;
      arrEditor['xVal'] = xval;
      arrEditor['yVal'] = yval;
      arrEditor['localProducts'] = localprds;
      arrEditor['worldName'] = this.worldName;
      arrEditor['idWorld'] = this.idWorld;
      arrEditor['gaName'] = gaName;
      //Se crea un array con la ultima estructura.
      let finalStructure = [];
      let aux = arrEditor['totalCords'] as Array<any>;
      finalStructure = arrEditor['Ycoords'];
      for(var i in finalStructure) {
        finalStructure[i] = aux.splice(0, xval);
      }
      arrEditor['finalStructure'] = finalStructure;

      // Se envía la estructura de la página
      this.mediator.sendStructure(arrEditor);
    }).catch(err=>{});
  }

  // Cierra Sidenav
  closeSidenav() {
    this.showSidenav = false;
  }

  //Traigo todo los tags
  getIconographies() {

    this.iconographyService.getIconographies2().then((data: {}) => {

      this.tags = data as Array<any>;
    }).catch(err=>{

    });
  }

}
