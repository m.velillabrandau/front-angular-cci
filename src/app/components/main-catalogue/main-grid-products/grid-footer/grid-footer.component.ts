import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IconographyService } from '../../../../services/iconography.service';
import { MediatorService } from '../../../../services/mediator.service';
import { GaService } from '../../../../services/ga.service';
import { WorldService } from '../../../../services/world.service';
import { SwalComponent } from '../../../shared/swal/swal.component';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../../environments/environment';
const PDFEndpoint = environment.UrlPdf; // ruta pdf

export interface Tags {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-grid-footer',
  templateUrl: './grid-footer.component.html',
  styleUrls: ['./grid-footer.component.css']
})
export class GridFooterComponent implements OnInit, OnDestroy {

  idWorldLocal: number = 0;
  public ag_page;
  public localWorld;
  FooterIcon: boolean = false;
  Cover: boolean = false;
  idFooterIcon: number;
  withCover: number;
  idcat: number = null;
  @Input() tags : Array<any> = [];
  @Input() idWorld : number;
  @Input() idPage : number;

  ruteTags = environment.URLTags; //rute tags

  constructor(private activeRouter: ActivatedRoute, private iconographyService: IconographyService, private mediator: MediatorService, private gaService: GaService, public worldService: WorldService) { }

  // Iniciación de clase
  ngOnInit() {

    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
		});
    this.mediator.getIdPage.subscribe(idpage => {
      this.getIconPage(idpage);
    })
    this.getIconPage(this.idPage);

    this.getCoverPage(this.idWorld);

  }
  ngOnDestroy(){
  }
  onChange(event) {

    let data = {
      idTag: event.value,
      id_cat: this.idcat
    }

    this.gaService.addIconFooterPage(this.idPage, data);

  }

  onChangeCover(event) {
    let with_cover;

    if(event.checked){
      with_cover =  1;
    }else{
      with_cover = 0;
    }

    this.worldService.editWorld(this.idWorld, {"with_cover": with_cover,"id_cat": this.idcat});
  }

  getIconPage(idPage){
    let tag_id = this.gaService.getIconPage(idPage).subscribe((data: {}) => {
      this.ag_page = data;
      if(data['tags_id'] != 0){
        this.FooterIcon = true;
        this.idFooterIcon = data['tags_id'];
      }else{
        this.idFooterIcon = 0;
      }
    });
  }

  getCoverPage(idWorldLocal){
    let tag_id = this.worldService.getLocalWorld(idWorldLocal).subscribe((data: {}) => {
      this.localWorld = data;
      if(data['with_cover'] != 0){
        this.Cover = true;
        this.withCover = data['with_cover'];
      }else{
        this.withCover = 0;
      }
    });
  }
  exportPDF() {
    window.open(PDFEndpoint + this.idcat+".pdf");
  }

}
