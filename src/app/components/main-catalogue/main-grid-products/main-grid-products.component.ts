import { Component, OnInit, Input } from '@angular/core';
import { MediatorService } from '../../../services/mediator.service';
@Component({
  selector: 'app-main-grid-products',
  templateUrl: './main-grid-products.component.html',
  styleUrls: ['./main-grid-products.component.css']
})
export class MainGridProductsComponent implements OnInit {
  @Input() idLocalGa: number = null;
  @Input() worldName: string = null;
  @Input() idWorld: string = null;
  @Input() idPage: number = null;
  @Input() products: Array<any> = [];
  @Input() structure: Array<any> = [];
  @Input() tags : Array<any> = [];
  @Input() contentsGroup : Array<any> = [];
  constructor(private mediator: MediatorService) { }
  ngOnInit(){
  }
}
