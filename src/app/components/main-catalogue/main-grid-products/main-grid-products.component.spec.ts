import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainGridProductsComponent } from './main-grid-products.component';

describe('MainGridProductsComponent', () => {
  let component: MainGridProductsComponent;
  let fixture: ComponentFixture<MainGridProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainGridProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainGridProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
