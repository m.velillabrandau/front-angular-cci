import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MediatorService } from '../../../../services/mediator.service';
import { GaService } from '../../../../services/ga.service';
import { SwalComponent } from './../../../../components/shared/swal/swal.component';
import { WorldService } from 'src/app/services/world.service';
import { MainCatalogueComponent } from '../../main-catalogue.component';
import { ActivatedRoute } from '@angular/router';
import { GridFooterComponent } from '../grid-footer/grid-footer.component';

@Component({
  selector: 'app-head-creation',
  templateUrl: './head-creation.component.html',
  styleUrls: ['./head-creation.component.css']
})
export class HeadCreationComponent implements OnInit {

  // worldName: string = null;
  public worldAliasFirstState:string = null;

  idPage: number = 0;
  pagesB: Array<any> = [];
  numberOfPages: number;
  orderPageCurrent: number = 1;
  existPages: number = 0;
  pageSelected;
  gridData: Array<any> = [];
  @Input() products: Array<any> = [];
  @Input() worldName: string = "";
  @Input() idWorld: number;
  @ViewChild("inputChild") el: ElementRef;
  idcat: number = null;

  constructor(private footer: GridFooterComponent, private activeRouter: ActivatedRoute, private mediator: MediatorService, private gaService: GaService, private main: MainCatalogueComponent, private worldService: WorldService) { }

  ngOnInit(){

    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
    });

    // Obtiene la data de la grilla
    this.mediator.StructureEvent.subscribe(structure => {
      this.gridData = structure;
      this.idPage = structure['idPage'];
      this.gaService.getPages(this.idPage).subscribe((data: {}) => {
        this.existPages = 1;
        this.pagesB = data as Array<any>;
        this.numberOfPages = this.pagesB.length
      });
     });

    this.mediator.setIdPage.subscribe(idPage => {
      this.idPage = idPage;

      if(this.idPage != 0){
        this.gaService.getPages(this.idPage).subscribe((data: {}) => {
          this.existPages = 1;
          this.pagesB = data as Array<any>;
          this.numberOfPages = this.pagesB.length
        });

      }else{
      }
    });
  }
  // Setea las dimensiones de la grilla
  setGrid(x,y){
    this.gaService.setDimentions(x, y, this.idPage, this.idcat).then(res => {
      console.log("ID PAGE:",this.idPage);
      console.log("X: ",x,"Y: ",y);
      console.log("GRID DATA: ", this.gridData);
      //Gatilla el redimensionado
      // this.mediator.sendChangueGrid(this.idPage);
      // console.log("RES: ",res);
      this.main.setState(this.gridData['productsRemote'], x, y, this.idPage, this.gridData['idLocalGa'],this.gridData['worldName'], this.gridData['gaName'], this.gridData['idWorld']);
      //Se procede a devolver a la grilla los productos restantes
      let max = x * y;
      let actual = 0;
      for(var i in this.gridData['Ycoords']) {
        for(var f in this.gridData['Ycoords'][i]) {
          actual++;
          if(actual > max && this.gridData['Ycoords'][i][f] != 'empty') {
            this.mediator.sendProductToList(this.gridData['Ycoords'][i][f]);
          }
        }
      }

    }).catch(err=> {
      SwalComponent.toast({
        type: 'error',
        title: 'Ha ocurrido un error al procesar la solicitud'
      });
    });
  }

  //obtener y almacener la variable de world_alias para conocer si tuvo cambios
  getWorldAliasFirstState(event:any){
    this.worldAliasFirstState = event.srcElement.value.trim()
  }
  //guarda el alias del mundo desde la grilla
  saveWorldAlias(event: any) {
    if (this.worldAliasFirstState.toUpperCase() != event.srcElement.value.trim().toUpperCase() ){
      //grabar mundo modificado
      this.worldService.setWorldAlias({ "world_id": this.idWorld, "world_alias" : event.srcElement.value.trim().toUpperCase(), "id_cat": this.idcat});
      this.mediator.reorderWorlds();
    }
  }

  saveWorldAliasOnKeyDown(event:any){
    if (event.key === "Enter") {
      if (this.worldAliasFirstState.toUpperCase() != event.srcElement.value.trim().toUpperCase() && this.idWorld != 0) {
        //grabar mundo modificado
        this.worldService.setWorldAlias({ "world_id": this.idWorld, "world_alias": event.srcElement.value.trim().toUpperCase() });
        this.mediator.reorderWorlds();
        this.el.nativeElement.blur();
      }
    }
  }

  addPage(){
    let data = {
      id_page: this.idPage,
      id_cat: this.idcat
    }
    this.gaService.addPage(data).then(res => {
      this.gaService.getPages(this.idPage).subscribe((data: {}) => {
        this.existPages = 1;
        this.pagesB = data as Array<any>;
        this.numberOfPages = this.pagesB.length
      });
    }).catch(err=> {

    });
  }

  deletePage(){
    if( this.pagesB.length > 1){
      let data = {
        id_page: this.idPage,
        id_cat: this.idcat
      }
      this.gaService.deletePage(data).then(res => {
        this.orderPageCurrent = 1;
        this.gaService.getPages(this.idPage).subscribe((data: {}) => {
          this.existPages = 1;
          this.pagesB = data as Array<any>;
          this.numberOfPages = this.pagesB.length
          let xval = this.pagesB[0]['x_value'];
          let yval = this.pagesB[0]['y_value'];
          this.main.setState(this.gridData['productsRemote'], xval, yval, this.pagesB[0]['id'], this.gridData['idLocalGa'],this.gridData['worldName'], this.gridData['gaName'], this.gridData['idWorld']);
        });
        
      }).catch(err=> {

      });
    } else {
      SwalComponent.toast({
        type: 'error',
        title: 'No puede eliminar la última página'
      });
    }

  }

  selectPage(idPageSelected, orderPage){
    this.orderPageCurrent = orderPage;
    let xval = 0;
    let yval = 0;
    for (var p in this.pagesB) {
      if(this.pagesB[p]['id'] == idPageSelected){
        xval = this.pagesB[p]['x_value'];
        yval = this.pagesB[p]['y_value'];
      }
    }
    // this.mediator.changePage(idPageSelected);
    // this.footer.getIconPage(idPageSelected);
    this.mediator.sendIdPageFooter(idPageSelected);
    this.main.setState(this.gridData['productsRemote'], xval, yval, idPageSelected, this.gridData['idLocalGa'],this.gridData['worldName'], this.gridData['gaName'], this.gridData['idWorld']);
  }
}
