import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombineModalComponent } from './combine-modal.component';

describe('CombineModalComponent', () => {
  let component: CombineModalComponent;
  let fixture: ComponentFixture<CombineModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombineModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombineModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
