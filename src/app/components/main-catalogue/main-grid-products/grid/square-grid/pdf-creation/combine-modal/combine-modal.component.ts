import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from "../../../../../../shared/dialog/dialog.component";
import { ProductService } from '../../../../../../../services/product.service';
@Component({
  selector: 'app-combine-modal',
  templateUrl: './combine-modal.component.html',
  styleUrls: ['./combine-modal.component.css']
})
export class CombineModalComponent implements OnInit {

  constructor(private prodService: ProductService, public dialogRef: MatDialogRef<CombineModalComponent>, public dialog: DialogComponent) { }
  data: Array<any> = [];

  ngOnInit() {
    this.data = this.dialog.getData();
  }

  onNoClick(): void {
    this.dialogRef.close('closed');
  }

  combine(){
    this.prodService.combine(this.data).then(res => {
      this.dialogRef.close('combined');
    }).catch(err => {
      this.dialogRef.close('err');
    });

  }
}
