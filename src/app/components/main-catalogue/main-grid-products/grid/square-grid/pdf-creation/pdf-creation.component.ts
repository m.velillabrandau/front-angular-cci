import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';
import { ProductService } from '../../../../../../services/product.service';
import { SwalComponent } from '../../../../../../components/shared/swal/swal.component';
import { MediatorService } from '../../../../../../services/mediator.service';
import { element } from '@angular/core/src/render3';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../../../../shared/dialog/dialog.component';
import { CombineModalComponent } from './combine-modal/combine-modal.component';
import { MainCatalogueComponent } from './../../../../main-catalogue.component';
import { GaService } from '../../../../../../services/ga.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pdf-creation',
  templateUrl: './pdf-creation.component.html',
  styleUrls: ['./pdf-creation.component.css']
})
export class PdfCreationComponent implements OnInit, OnDestroy{
  @Input() xval: Array<any> = [];
  @Input() gridData: Array<any>;
  @Input() indexX: number = 0;
  @Input() indexY: number = 0;
  @Input() tags : Array<any> = [];
  moreSku: Array<any> = [];
  exist: boolean = false;
  class: string = 'fromGrid';
  setState: Array<any> = []; // Este array es para que los productos puedan recargar
  subs = new Subscription(); //Eventos de Dragula
  dsubs: Subscription;
  idcat: number = null;
  constructor(private mediator: MediatorService, private activeRouter: ActivatedRoute, private main: MainCatalogueComponent, public dialog: MatDialog, private dragulaService: DragulaService, private productService: ProductService) {}
  ngOnDestroy() {
    this.dsubs.unsubscribe();
  }
  ngOnInit(){
    //ID DEL CATALOGO
    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
    });

    // Existe un producto en una grilla
    if(typeof this.xval == 'object') {
      this.exist = true;
      this.setState = [this.gridData['productsRemote'], this.gridData['xVal'], this.gridData['yVal'], this.gridData['idPage'], this.gridData['idLocalGa']];
      // Se comprueban los hijos y se agregan
      for( var i in this.xval['soons']) {
        let isnew = "cursor: pointer; margin: 1px;";
        if(this.xval['soons'][i]['is_new'] == 1) {
         isnew = "cursor: pointer; margin: 1px;background-color: yellow";
        }
        this.moreSku.push([this.xval['soons'][i]['sku'], this.xval['soons'][i]['measurements'], this.xval['soons'][i]['pp_symbol'], this.xval['soons'][i]['is_new'],this.xval['soons'][i]['measurements'], isnew, this.xval['soons'][i]['punta_precio']]);
      }


    }
    //Cuando se arrastra un producto en la grilla
    this.dsubs = this.subs.add(this.dragulaService.drop("products")
      .subscribe(({ target, el, source }) => {
        // Si se draga en la posición de la grilla actual se debe guardar
        if(target.classList.value == "block dash grid-container pos-"+this.indexY+this.indexX) {
          // Le indica que está en una grilla y no en un listado por lo que se adecua al tamaño de su nuevo padre
          el.removeAttribute('class');
          el.setAttribute('class', 'mat-card fromGrid');
          // Se declara lo necesario para guardar
          if(target.childElementCount < 2 ) { // No hay una targeta en la grilla, no se debe unir
            let contentToSave = {
              sku: el.getAttribute('data-sku'),
              localGa:  this.gridData['idLocalGa'],
              corder: this.gridData['xVal'] * +this.indexY + this.indexX + 1,
              idPage: el.getAttribute('data-idpage'),
              madreid: el.getAttribute('data-madreid'),
              etariocod: el.getAttribute('data-etariocod'),
              codn3: el.getAttribute('data-codn3'),
              codn4: el.getAttribute('data-codn4'),
              idCat: this.idcat
            }
            if(contentToSave.sku == null) {
              contentToSave = {
                sku: el.firstElementChild.getAttribute('data-sku'),
                localGa: this.gridData['idLocalGa'],
                corder: this.gridData['xVal'] * +this.indexY + this.indexX + 1,
                idPage: el.firstElementChild.getAttribute('data-idpage'),
                madreid: el.firstElementChild.getAttribute('data-madreid'),
                etariocod: el.firstElementChild.getAttribute('data-etariocod'),
                codn3: el.firstElementChild.getAttribute('data-codn3'),
                codn4: el.firstElementChild.getAttribute('data-codn4'),
                idCat: this.idcat
              }
            }
            // Se guarda la posición del contenido en bd
            this.productService.saveContent(contentToSave).then(res => {
              this.main.setState(this.gridData['productsRemote'], this.gridData['xVal'], this.gridData['yVal'], this.gridData['idPage'], this.gridData['idLocalGa'], this.gridData['worldName'], this.gridData['gaName'] ,this.gridData['idWorld']);
            }).catch(err => {
              SwalComponent.toast({
                type: 'error',
                title: 'Ha ocurrido un error al procesar la solicitud'
              });
            });
          } else {
            // Se deben unir los contenidos
            let body = {
              sku: el.firstElementChild.getAttribute('data-sku'),
              localGa: this.gridData['idLocalGa'],
              corder: this.gridData['xVal'] * +this.indexY + this.indexX + 1,
              idPage: el.firstElementChild.getAttribute('data-idpage'),
              madreid: el.firstElementChild.getAttribute('data-madreid'),
              etariocod: el.firstElementChild.getAttribute('data-etariocod'),
              codn3: el.firstElementChild.getAttribute('data-codn3'),
              codn4: el.firstElementChild.getAttribute('data-codn4'),
              name: el.firstElementChild.getAttribute('data-name'),
              punta_precio: el.firstElementChild.getAttribute('data-punta_precio'),
              diseno: el.firstElementChild.getAttribute('data-diseno'),
              color: el.firstElementChild.getAttribute('data-color'),
              idCat: this.idcat
            }
            if(body.sku == null) {
              body = {
                name: el.getAttribute('data-name'),
                punta_precio: el.getAttribute('data-punta_precio'),
                diseno: el.getAttribute('data-diseno'),
                color: el.getAttribute('data-color'),
                sku: el.getAttribute('data-sku'),
                localGa: this.gridData['idLocalGa'],
                corder: this.gridData['xVal'] * +this.indexY + this.indexX + 1,
                idPage: el.getAttribute('data-idpage'),
                madreid: el.getAttribute('data-madreid'),
                etariocod: el.getAttribute('data-etariocod'),
                codn3: el.getAttribute('data-codn3'),
                codn4: el.getAttribute('data-codn4'),
                idCat: this.idcat
              }
            }
            // el.remove();
            // if(typeof el.firstElementChild.firstElementChild.children[1].children[1] === "undefined"){
            this.openModalCombine(body);
            // }

          }
        }

    }))
  }
  openModalCombine(body): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {component: CombineModalComponent, title: "Combinar productos", body: body}
    });
    dialogRef.afterClosed().subscribe(result => {
        // this.mediator.changePage(this.idPage);
      if(result == 'closed') {
        console.log('borrar body', body);
        body = {
          codn3: body['codn3'],
          codn4: body['codn4'],
          color: body['color'],
          diseno: body['diseno'],
          punta_precio: body['punta_precio'],
          etariocod: body['etariocod'],
          madre_id: body['madreid'],
          name: body['name'],
          sku: body['sku'],
        }
        this.mediator.sendProductToList(body);
      }
      this.main.setState(this.gridData['productsRemote'], this.gridData['xVal'], this.gridData['yVal'], this.gridData['idPage'], this.gridData['idLocalGa'],this.gridData['worldName'], this.gridData['gaName'], this.gridData['idWorld']);
    });
  }
}
