import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from "../../../../../../shared/dialog/dialog.component";
import { ProductService } from '../../../../../../../services/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SwalComponent } from '../../../../../../shared/swal/swal.component';

@Component({
  selector: 'app-optional-text',
  templateUrl: './optional-text.component.html',
  styleUrls: ['./optional-text.component.css']
})
export class OptionalTextComponent implements OnInit {
  OptionalTextForm: FormGroup;
  constructor(private prodService: ProductService, public dialogRef: MatDialogRef<OptionalTextComponent>, public dialog: DialogComponent) {
    dialogRef.disableClose = true;
  }
  data: Array<any> = [];
  optionalText: string = "";
  markTag: string ="";
  markLess: string ="";
  markMore: string ="";
  markNew: string ="";
  markSku: string ="";
  ngOnInit() {
    this.data = this.dialog.getData();


    this.setValueProduct();
    this.OptionalTextForm = new FormGroup({
      'optional': new FormControl(null,[
        Validators.required,
        Validators.maxLength(255)
      ])
    });
  }
  onNoClick(): void {
    this.dialogRef.close(['closed', '' ,this.data]);
  }
  save() {
  }
  onOptionalKeyup(event:any){
		this.optionalText = event.target.value;
  }
  onSubmit() {
    if (this.OptionalTextForm.valid) {

      this.prodService.saveOptionalTextProduct(this.data['sku'], {pageId: this.data['pageId'], optionalText:this.optional.value, id_cat: this.data['idcat']}).then(res=> {
      SwalComponent.toast({
        type: 'success',
        title: 'Texto opcional guardado'
      });
      this.dialogRef.close(['success', this.optional.value, this.data]);
    }).catch(err => {
      SwalComponent.toast({
        type: 'error',
        title: 'Ha ocurrido un error al procesar la solicitud'
      });
      this.dialogRef.close('error');
     })
    }
  }
  // Valores del las acciones del producto
  giveValue(typeValue){
    event.preventDefault();
    let data = {
      typeValue: typeValue,
      pageId: this.data['pageId'],
      id_cat: this.data['idcat']
    }
    this.prodService.setValueProduct(this.data['sku'], data).then(res=> {

      if(res['pp_symbol'] == 1 ){
        this.markMore = "green";
        this.markLess = "black";
        this.data['ppVal'] = 1;
      }

      if(res['pp_symbol'] == 2 ){
        this.markLess = "green";
        this.markMore = "black";
        this.data['ppVal'] = 2;
      }

      if(res['pp_symbol'] == 0 ){
        this.markMore = "black";
        this.markLess = "black";
        this.data['ppVal'] = 0;
      }

      if ( (res['is_new'] == 1 )  ){
        this.markNew = "green";
        this.markSku = "yellow";
        this.data['isnew'] = 1;

      }else{
        this.markNew = "black";
        this.markSku = "white";
        this.data['isnew'] = 0;
      }
    });
  }
  // Setea por defecto los valores + - ☆ del producto
  setValueProduct(){
    if(this.data['ppVal'] == 1 ){
      this.markMore = "green";
    }

    if(this.data['ppVal'] == 2 ){
      this.markLess = "green";
    }

    if ( (this.data['isnew'] != 0 )&& (this.data['isnew'] != undefined) && (this.data['isnew'] != null) ){
      this.markNew = "green";
      this.markSku = "yellow";
    }
  }
  get optional() { return this.OptionalTextForm.get('optional'); }
  matcher = new ErrorStateMatcher();
}
