import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionalTextComponent } from './optional-text.component';

describe('OptionalTextComponent', () => {
  let component: OptionalTextComponent;
  let fixture: ComponentFixture<OptionalTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionalTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionalTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
