import { Component, OnInit, Input } from '@angular/core';
import { CatalogueService } from '../../../../../../services/catalogue.service';

@Component({
  selector: 'app-superior-labels',
  templateUrl: './superior-labels.component.html',
  styleUrls: ['./superior-labels.component.css']
})
export class SuperiorLabelsComponent implements OnInit{
  @Input() idPage: number = null;
  @Input() yVal: number;
  @Input() contentsGroup : Array<any> = [];

  constructor(private catalogueService: CatalogueService) { }

  ngOnInit(){
  }


}
