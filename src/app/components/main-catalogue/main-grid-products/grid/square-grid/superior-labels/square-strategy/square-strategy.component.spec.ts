import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquareStrategyComponent } from './square-strategy.component';

describe('SquareStrategyComponent', () => {
  let component: SquareStrategyComponent;
  let fixture: ComponentFixture<SquareStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquareStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquareStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
