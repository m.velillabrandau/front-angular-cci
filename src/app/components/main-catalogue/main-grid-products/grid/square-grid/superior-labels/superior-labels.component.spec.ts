import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperiorLabelsComponent } from './superior-labels.component';

describe('SuperiorLabelsComponent', () => {
  let component: SuperiorLabelsComponent;
  let fixture: ComponentFixture<SuperiorLabelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperiorLabelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperiorLabelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
