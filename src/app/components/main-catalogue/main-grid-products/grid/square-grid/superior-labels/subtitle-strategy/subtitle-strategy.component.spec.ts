import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtitleStrategyComponent } from './subtitle-strategy.component';

describe('SubtitleStrategyComponent', () => {
  let component: SubtitleStrategyComponent;
  let fixture: ComponentFixture<SubtitleStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubtitleStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtitleStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
