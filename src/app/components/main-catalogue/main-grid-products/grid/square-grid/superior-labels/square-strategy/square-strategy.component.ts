import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MediatorService } from '../../../../../../../services/mediator.service';
import { GaService } from '../../../../../../../services/ga.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CatalogueService } from '../../../../../../../services/catalogue.service';
import { empty } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-square-strategy',
  templateUrl: './square-strategy.component.html',
  styleUrls: ['./square-strategy.component.css']
})
export class SquareStrategyComponent implements OnInit, OnDestroy{
  squareStrategyAddForm: FormGroup;
  @Input() idPage: number = null;
  @Input() yVal: number;
  squareStrategy: string = "";
  @Input() contentsGroup : Array<any> = [];
  idcat: number = null;

  constructor(private activeRouter: ActivatedRoute, private mediator: MediatorService,  private gaService: GaService, private catalogueService: CatalogueService) { }
  ngOnDestroy() {
    this.squareStrategy = "";
  }
  ngOnInit(){

    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
    });

    this.squareStrategyAddForm = new FormGroup({
      'square_strategy': new FormControl(null,[
      ])
    });

    if(typeof this.contentsGroup === "undefined"){
      this.squareStrategy = "";
    }else{
      this.squareStrategy = this.contentsGroup['title'];
    }

  }

  //obtener y almacener la variable de cuadro de estrategia
  getSquareStrategyFirstState(event:any){
    this.squareStrategy = event.srcElement.value.trim()
  }
  //guarda cuadro de estrategia de la pagina
  saveSquareStrategy(event: any) {
    if (this.squareStrategy.toUpperCase() != event.srcElement.value.trim().toUpperCase()){
      //grabar mundo modificado
      if (this.squareStrategyAddForm.valid) {
        let data = {
          squareStrategy: this.squareStrategyAddForm.value.square_strategy,
          pageId: this.idPage,
          yVal: this.yVal + 1,
          id_cat: this.idcat
        }
        this.gaService.addSquareStrategy(data).then(res=> {

        });
      }
    }
  }
}
