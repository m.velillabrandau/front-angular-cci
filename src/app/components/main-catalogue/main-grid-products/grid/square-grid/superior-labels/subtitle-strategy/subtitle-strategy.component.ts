import { Component, OnInit, Input } from '@angular/core';
import { MediatorService } from '../../../../../../../services/mediator.service';
import { GaService } from '../../../../../../../services/ga.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subtitle-strategy',
  templateUrl: './subtitle-strategy.component.html',
  styleUrls: ['./subtitle-strategy.component.css']
})
export class SubtitleStrategyComponent {
  subtitleStrategyAddForm: FormGroup;
  @Input() idPage: number = null;
  @Input() yVal: number;
  subtitleStrategy: string = "";
  @Input() contentsGroup : Array<any> = [];
  idcat: number = null;

  constructor(private activeRouter: ActivatedRoute, private gaService: GaService) { }

  ngOnInit(){

    this.activeRouter.params.subscribe(parametros=>{
      if(parametros["id"] != null){
        this.idcat = parametros["id"];
      }
    });

    this.subtitleStrategyAddForm = new FormGroup({
      'subtitle_strategy': new FormControl(null,[
      ])
    });

    if(typeof this.contentsGroup === "undefined"){
      this.subtitleStrategy = "";
    }else{
      this.subtitleStrategy = this.contentsGroup['subtitle'];
    }
  }


  //obtener y almacener la variable subtitulo de estrategia
  getSubtitleStrategyFirstState(event:any){
    this.subtitleStrategy = event.srcElement.value.trim()
  }

  //guarda el subtitulo de estrategia
  saveSubtitleStrategy(event: any) {
    if (this.subtitleStrategy.toUpperCase() != event.srcElement.value.trim().toUpperCase()){
      //grabar mundo modificado
      if (this.subtitleStrategyAddForm.valid) {
        let data = {
          subtitleStrategy: this.subtitleStrategyAddForm.value.subtitle_strategy,
          pageId: this.idPage,
          yVal: this.yVal + 1,
          id_cat: this.idcat
        }
        this.gaService.addSubtitleStrategy(data).then(res=> {
        });
      }
    }
  }

}
