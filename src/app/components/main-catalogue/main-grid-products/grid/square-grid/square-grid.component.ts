import { Component, OnInit, Input } from '@angular/core';
import { MediatorService } from '../../../../../services/mediator.service';
@Component({
  selector: 'app-square-grid',
  templateUrl: './square-grid.component.html',
  styleUrls: ['./square-grid.component.css']
})
export class SquareGridComponent implements OnInit {
  @Input() yCoord: number;
  @Input() gridData: Array<any>;
  @Input() indexY: number = 0;
  @Input() tags : Array<any> = [];
  @Input() contentsGroup : Array<any> = [];
  
  constructor(private mediator: MediatorService) { }

  ngOnInit(){
  }
}
