import { Component, Input, OnInit } from '@angular/core';
import { MediatorService } from '../../../../services/mediator.service';
import { OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnDestroy {
  productStructure: Array<any> = [];
  gridData: Array<any> = [];
  sub: Subscription;
  @Input() tags : Array<any> = [];
  contentsGroup : Array<any> = [];

  constructor(private mediator: MediatorService) {}
  ngOnInit() {
    this.sub = this.mediator.StructureEvent.subscribe(structure => {
      this.productStructure = [];
      this.gridData = [];
      this.productStructure  = structure['finalStructure'];
      this.gridData = structure;
      this.contentsGroup = structure['contentsGroups'];
    });
  }
  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}
