import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WorldService } from '../../../services/world.service';
import { CatalogueService } from '../../../services/catalogue.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { MediatorService } from '../../../services/mediator.service';
@Component({
  selector: 'app-list-world',
  templateUrl: './list-world.component.html',
  styleUrls: ['./list-world.component.css']
})
export class ListWorldComponent implements OnInit {
  idCat: number;
  idFeria: number;
  worlds: Array<any> = [];
  worldsLocal: Array<any> = [];
  constructor(private mediator: MediatorService, private activeRouter: ActivatedRoute, public worldService: WorldService, public catService: CatalogueService) { }
  ngOnInit() {
    // Obtiene parametros de la ruta
    this.activeRouter.params.subscribe(params=>{
      if(params["id"] != null) {
        this.idCat = params["id"];
        // Mundos local
        this.getWorldsLocal(this.idCat);
        // Mundos base casa ideas
        this.getWorlds(this.idCat);
      }
    });
    // Se suscribe para ordenar el mundo
    this.mediator.changueOrderWorld.subscribe(bol => {
      this.worldsLocal = [];
      this.worlds = [];
      this.getWorldsLocal(this.idCat);
      this.getWorlds(this.idCat);
    })
  }


  // Obtiene los mundos guardados en la base de datos local, si exisitiesen
  public getWorldsLocal(idCat) {
    this.worldService.getWorldsLocal(idCat).subscribe((data: {}) => {

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          // El array es ['idCorpo' => 'idLocal']
          this.worldsLocal[data[key]['nivel1_id'] + '-' + data[key]['nivel2_id']] = [data[key]['id'], data[key]['w_order'], data[key]['wld_alias']];
        }
      }
    });
  }
  // Obtiene los mundos
  public getWorlds(idCat) {
    // Se debe ir a buscar el catálogo para traer el id de la feria con que fue creado
    this.catService.getCataloge(idCat).subscribe((data: {})=> {
      // Se va a buscar el contenido del catálogo
      this.catService.getContentMagazine(data['ferias_id']).subscribe((contentMagazine: {}) => {
        let productosCaja = []; // Los productos caja
        for (var key in contentMagazine) {
          if (contentMagazine.hasOwnProperty(key)) {
            for(let ga in contentMagazine[key]['world']['ga']) {
              if(contentMagazine[key]['world']['ga'][ga]['name'] == "PRODUCTOS CAJA") {
                for(var prod in contentMagazine[key]['world']['ga'][ga]['products']) {
                  productosCaja.push(contentMagazine[key]['world']['ga'][ga]['products'][prod]);
                }
              }
            }
            let exist = 0;
            let idLocal  = 0;
            let order= null;
            let disable = true;
            //Si existe el mundo entonces se setea exist en 1
            if (key in this.worldsLocal) {
              exist = 1;
              idLocal = this.worldsLocal[key][0];
              order = this.worldsLocal[key][1];
              contentMagazine[key]['world']['name'] = this.worldsLocal[key][2];
              disable = false;
            }
            this.worlds.push({disable: disable, orderValue: order, value: key, viewValue: contentMagazine[key]['world']['name'], exist: exist, gas: contentMagazine[key]['world']['ga'], idCatalogue:idCat, idWorldLocal: idLocal});
          }
        }
          // Se agrega el mundo producto caja
          // Propiedades mundo productos caja
          let ordervalueC = null;
          let disableC = true;
          let existC = 0;
          let idWorldLocalC = 0;
          let nameC = "PRODUCTOS CAJA";
          // -- //
          for(var caja in this.worldsLocal) {
            if(caja == "99999-99999") {
              ordervalueC = this.worldsLocal[caja][1];
              disableC = false;
              existC = 1;
              idWorldLocalC = this.worldsLocal[caja][0];
              nameC = this.worldsLocal[caja][2];
            }
          }
          //Se suma a los demás mundos
          this.worlds.push({disable: disableC, orderValue: ordervalueC, value: '99999-99999', viewValue: nameC, exist: existC, gas: {99999:{id:99999, name: 'PRODUCTOS CAJA', products:productosCaja}}, idCatalogue:this.idCat, idWorldLocal: idWorldLocalC});
        // Orden de los mundos
        this.worlds.sort((a,b) => (a.orderValue > b.orderValue) ? 1 : ((b.orderValue > a.orderValue) ? -1 : 0));
        this.worlds.sort((a,b) => (a.exist < b.exist) ? 1 : ((b.exist < a.exist) ? -1 : 0));
      })
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.worlds, event.previousIndex, event.currentIndex);
    // Se reordena en base de datos consumiendo el correspondiente servicio
    let body = {
      idWorld: event.item.element.nativeElement.getAttribute('ng-reflect-id-world-local'),
      idCat: this.idCat,
      position: +event.currentIndex,
      prevPosition: event.previousIndex
    }
    this.worldService.orderProducts(body).then(res => {
      //se reordena
      for (var i in this.worlds) {
        if(this.worlds[i]['idWorldLocal'] > 0) {
          for(var x in res) {
            if(res[x]['id'] == this.worlds[i]['idWorldLocal']) {
              this.worlds[i]['orderValue'] = res[x]['w_order'];
            }
          }
        }
      }
    }).catch(err=> {
      //err
    });
  }
}
