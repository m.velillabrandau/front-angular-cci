import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldSnackBarComponent } from './world-snack-bar.component';

describe('WorldSnackBarComponent', () => {
  let component: WorldSnackBarComponent;
  let fixture: ComponentFixture<WorldSnackBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldSnackBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
