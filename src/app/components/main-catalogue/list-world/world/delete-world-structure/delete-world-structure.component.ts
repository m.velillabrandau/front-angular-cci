import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from "../../../../shared/dialog/dialog.component";
import { WorldService } from '../../../../../services/world.service';
import { MediatorService } from '../../../../../services/mediator.service';

@Component({
  selector: 'app-delete-world-structure',
  templateUrl: './delete-world-structure.component.html',
  styleUrls: ['./delete-world-structure.component.css']
})
export class DeleteWorldStructureComponent implements OnInit {
  data: Array<any> = [];
  constructor(private mediator: MediatorService, private worldService: WorldService , public dialogRef: MatDialogRef<DeleteWorldStructureComponent>, public dialog: DialogComponent) { }

  ngOnInit() {
    this.data = this.dialog.getData();
  }
  onNoClick(): void {
    this.dialogRef.close('closed');
  }
  deleteWorld() {
    this.worldService.deleteStructure(this.data['idcatalogue'], this.data['idWorld']).then(response => {
      this.mediator.reorderWorlds();
      this.dialogRef.close('deleted');
    }).catch(e => {
        console.log(e);
        this.dialogRef.close('error');
    });

  }
}
