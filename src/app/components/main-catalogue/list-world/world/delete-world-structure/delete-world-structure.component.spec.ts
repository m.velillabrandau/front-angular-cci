import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteWorldStructureComponent } from './delete-world-structure.component';

describe('DeleteWorldStructureComponent', () => {
  let component: DeleteWorldStructureComponent;
  let fixture: ComponentFixture<DeleteWorldStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteWorldStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteWorldStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
