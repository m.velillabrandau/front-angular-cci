import { Component, Input, OnInit } from '@angular/core';
import { GaService } from '../../../../services/ga.service';
import { WorldService } from '../../../../services/world.service';
import { ActivatedRoute } from '@angular/router';
import { DeleteWorldStructureComponent } from './delete-world-structure/delete-world-structure.component';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { WorldSnackBarComponent } from './world-snack-bar/world-snack-bar.component';
import { MatSnackBar } from '@angular/material';
import { MediatorService } from '../../../../services/mediator.service';
@Component({
  selector: 'app-world',
  templateUrl: './world.component.html',
  styleUrls: ['./world.component.css']
})
export class WorldComponent implements OnInit {
  @Input() name : string = null;
  @Input() idWorld : number = null;
  @Input() idCatalogue : number = null;
  @Input() exist : number = null;
  @Input() gas : any = [];
  @Input() idWorldLocal : number = null;

  idCat: number;
  tooltip: string; // Tooltip al momento de pasar el ratón sobre el ícono
  loadIcon: string; // Ícono de carga o eliminación de mundo en base de datos local
  constructor(private mediator: MediatorService, public snackBar: MatSnackBar, public dialog: MatDialog, private gaService: GaService, private worldService: WorldService, private activeRouter: ActivatedRoute) { }
  ngOnInit() {
    this.activeRouter.params.subscribe(parametros=>{
			if(parametros["id"] != null){
				this.idCat = parametros["id"];
      }
    });
    this.tooltip = "Añadir al Catálogo";
    this.loadIcon = "archive";
    // Si existe el mundo en la base de datos local, entonces se cambia el ícono y el tooltip por uno para eliminación
    if(this.exist) {
      this.loadIcon = "delete_forever";
      this.tooltip = "Eliminar del Catálogo";
    }
  }
  loadGas() {
    if(this.exist) {
      this.gaService.toggleWorld(this.gas, this.idWorld, this.idCatalogue, this.idWorldLocal);
      this.mediator.sendIdWorldLocal(this.idWorldLocal);
      this.mediator.sendWorldNameLocal(this.name);

    } else {
      this.openSnackBar();
    }
  }
  addOrRemoveWorld() {
    //Si existe previamente el mundo
    if(this.exist) {
      this.openModalDeleteWorldStructure();
    } else {
      //No existe un mundo por tanto se debe crear de forma local
      let data = [{'id_category': this.idCat ,'name': this.name, 'idWorld': this.idWorld, 'gas': this.gas}];
      this.worldService.createStructure(data[0]).then(res=> {
        this.idWorldLocal = +res;
        this.loadGas();
      });
      this.loadIcon = "delete_forever";
      var element = document.getElementById("card-world-"+this.idWorld);
      element.classList.add("show-red");
      this.exist = 1;
      this.tooltip = "Eliminar del Catálogo";
    }

  }
  // Se encarga de marcar en rojos los mundos que existan en la bd local
  setExistCard() {
    let classes = {
      'show-red': this.exist
    };
    return classes;
  }
  //Abre el modal para eliminar estructuras de mundos
  openModalDeleteWorldStructure(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {component: DeleteWorldStructureComponent, title: "Quitar mundo del Catálogo", idWorld:this.idWorld, idcatalogue: this.idCat}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == 'deleted') {
        //Quita el color rojo
        this.exist = 0;
        this.tooltip = "Añadir al Catálogo";
        this.loadIcon = "archive";
        this.idWorldLocal = 0;
        this.gaService.toggleWorld(this.gas, this.idWorld, this.idCatalogue, 0);
      }
    });
  }
  //Snackbar que aparece cuando clickean un Mundo sin haberlo agregado
  openSnackBar() {
    this.snackBar.openFromComponent(WorldSnackBarComponent, {
      duration: 3000,
    });
  }
}
