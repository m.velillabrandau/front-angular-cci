import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWorldComponent } from './list-world.component';

describe('ListWorldComponent', () => {
  let component: ListWorldComponent;
  let fixture: ComponentFixture<ListWorldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWorldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWorldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
