import { Component, OnInit } from '@angular/core';
import { GaService } from '../../../services/ga.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { MediatorService } from '../../../services/mediator.service';
@Component({
  selector: 'app-ga-list',
  templateUrl: './ga-list.component.html',
  styleUrls: ['./ga-list.component.css']
})
export class GaListComponent implements OnInit {
  constructor(private mediator: MediatorService, private gaService: GaService) { }
  gas: Array<any> = [];
  gasLocal: Array<any> = [];
  idlocalWorld: number = 0;
  normalGas: any = null;
  worldName:string = null;
  ngOnInit() {
  // Obtención del id del mundo guardado en la base de datos local
    this.gaService.change.subscribe(gas=> {

      this.gas  = [];
      this.gasLocal  = [];
      this.normalGas = gas;
      let first = true
      for (var key in gas) {
        if (gas.hasOwnProperty(key)) {
          if (first) { //Se obtiene el id del world local
            this.idlocalWorld = gas[key]['idWorldLocal']
            first = false;
          }
        }
      }
      // Se obtiene los ga locales
      if(this.idlocalWorld > 0){
        this.gaService.loadLocalGas(this.idlocalWorld).then(data => {
          let dataLocal = [];
          for (var keyS in data) {
            if (data.hasOwnProperty(keyS)) {
              dataLocal[data[keyS]['grupoanalisis_id']] = [data[keyS]['id'], data[keyS]['ag_order'], data[keyS]['wld_alias'] ];
            }
          }
          for (var key in gas) {
            if (gas.hasOwnProperty(key)) {
              let exist = 0;
              let idLocal = 0;
              let order= null;
              if (key in dataLocal) {
                exist = 1;
                idLocal = dataLocal[key][0];
                order = dataLocal[key][1];
              }
              this.gas.push({orderValue: order, value: key, viewValue: gas[key]['name'], worldName: gas[key]['worldName'], products: gas[key]['products'], idWorld: gas[key]['idWorld'], idCatalogue:gas[key]['idCatalogue'], exist: exist, idGaLocal:idLocal });
            }
          }
          this.gas.sort((a,b) => (a.viewValue > b.viewValue) ? 1 : ((b.viewValue > a.viewValue) ? -1 : 0));
          this.gas.sort((a,b) => (a.orderValue > b.orderValue) ? 1 : ((b.orderValue > a.orderValue) ? -1 : 0));
          this.gas.sort((a,b) => (a.exist < b.exist) ? 1 : ((b.exist < a.exist) ? -1 : 0));
        })
      }
    });
    this.mediator.changueOrderGa.subscribe(bol => {
      this.gaService.toggleWorld(this.normalGas, this.gas[0]['idWorld'], this.gas[0]['idCatalogue'],  this.idlocalWorld);
    })
    this.mediator.changeWorldNameLocal.subscribe(worldNameLocal => {
      if (worldNameLocal) {
        this.worldName = worldNameLocal;
      }
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.gas, event.previousIndex, event.currentIndex);
    let body = {
      idWorld: this.idlocalWorld,
      idGa: event.item.element.nativeElement.getAttribute('ng-reflect-id-ga-local'),
      position: +event.currentIndex,
      prevPosition: event.previousIndex
    }
    this.gaService.orderGas(body).then(res => {
      this.gaService.toggleWorld(this.normalGas, this.gas[0]['idWorld'], this.gas[0]['idCatalogue'],  this.idlocalWorld);
    }).catch(err=> {
      //err
    });
  }
}
