import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaListComponent } from './ga-list.component';

describe('GaListComponent', () => {
  let component: GaListComponent;
  let fixture: ComponentFixture<GaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
