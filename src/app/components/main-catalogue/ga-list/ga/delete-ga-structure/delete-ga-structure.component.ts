import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DialogComponent } from "../../../../shared/dialog/dialog.component";
import { GaService } from '../../../../../services/ga.service';
import { SwalComponent } from '../../../../../components/shared/swal/swal.component';

@Component({
  selector: 'app-delete-ga-structure',
  templateUrl: './delete-ga-structure.component.html',
  styleUrls: ['./delete-ga-structure.component.css']
})
export class DeleteGaStructureComponent implements OnInit {
  data: Array<any> = [];
  constructor(private gaService: GaService , public dialogRef: MatDialogRef<DeleteGaStructureComponent>, public dialog: DialogComponent) { }

  ngOnInit() {
    this.data = this.dialog.getData();
  }
  onNoClick(): void {
    this.dialogRef.close('closed');
  }
  deleteGa() {

    this.gaService.removeGa(this.data['id']).then(response => {
      this.dialogRef.close('deleted');
      SwalComponent.toast({
        type: 'success',
        title: 'GA eliminado del Catálogo'
      });
    }).catch(e => {
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido eliminar el GA del Catálogo, por favor intente nuevamente'
        });
        this.dialogRef.close('error');
    });

  }

}
