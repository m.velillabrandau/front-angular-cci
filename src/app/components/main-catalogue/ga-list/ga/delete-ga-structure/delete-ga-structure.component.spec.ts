import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteGaStructureComponent } from './delete-ga-structure.component';

describe('DeleteGaStructureComponent', () => {
  let component: DeleteGaStructureComponent;
  let fixture: ComponentFixture<DeleteGaStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteGaStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteGaStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
