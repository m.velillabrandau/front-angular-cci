import { Component, Input, OnInit } from '@angular/core';
import { MainCatalogueComponent } from '../../main-catalogue.component';
import { MediatorService } from '../../../../services/mediator.service';
import { MatSnackBar } from '@angular/material';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { GaService } from '../../../../services/ga.service';
import { SwalComponent } from './../../../../components/shared/swal/swal.component';
import { DeleteGaStructureComponent } from './delete-ga-structure/delete-ga-structure.component';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { IconographyService } from '../../../../services/iconography.service';

@Component({
  selector: 'app-ga',
  templateUrl: './ga.component.html',
  styleUrls: ['./ga.component.css']
})

export class GaComponent implements OnInit {
  sideNav: Boolean;
  @Input() name : string = null;
  @Input() idGa : number = null;
  @Input() products : any = [];
  @Input() idWorld : number = null;
  @Input() exist : number = null;
  @Input() idCatalogue : number = null;
  @Input() idGaLocal : number = null;
  @Input() worldName:string = null;
  @Input() idlocalWorld : number = null;

  tooltip: string; // Tooltip al momento de pasar el ratón sobre el ícono
  loadIcon: string; // Ícono de carga o eliminación de mundo en base de datos local

  tags: Array<any> = [];

  constructor(private iconographyService: IconographyService, public dialog: MatDialog, private gaService: GaService, private mediator: MediatorService, private main: MainCatalogueComponent, public snackBar: MatSnackBar ) { }

  // Configuración sideNav
  setSideNavState() {
    if(this.exist) {
      // Se obtiene la primera página del catálogo para renderizar
      this.gaService.getFirstPage(this.idGaLocal).then(res => {
        //se envía al set state los productos y las dimensiones de la grilla de la primera página
        this.main.setState(this.products, res['x_value'], res['y_value'], res['id'], this.idGaLocal, this.worldName, this.name, this.idlocalWorld);
      }).catch(err => {
        SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
      });
    } else {
      this.openSnackBar();
    }
  }
  ngOnInit() {
    this.mediator.reloadGridE.subscribe(res=>{
      if(this.exist){
        this.gaService.getPageP(res).then(res => {
          //se envía al set state los productos y las dimensiones de la grilla de la primera página
          this.main.setState(this.products, res['x_value'], res['y_value'], res['id'], this.idGaLocal, this.worldName, this.name);

        }).catch(err => {
          SwalComponent.toast({
            type: 'error',
            title: 'Ha ocurrido un error al procesar la solicitud'
          });
        });
      }
    });
    // Detecta cambios en las dimensiones de la grilla
    this.mediator.changeGrid.subscribe(res => {
      if(res == this.idGaLocal) { // Si el cambio corresponde a la grilla renderizada actualmente la refresca
        this.setSideNavState();
      }
    });
    this.tooltip = "Añadir al Catálogo";
    this.loadIcon = "archive";
    if(this.exist) {
      this.loadIcon = "delete_forever";
      this.tooltip = "Eliminar del Catálogo";
    }
  }
  //Agrega o elimina un ga
  addOrRemoveGa() {
    event.stopPropagation();
    if(this.exist){
      this.openModalDeleteGaStructure(this.idGaLocal);
    } else {
      this.gaService.loadGa(this.idGa, this.name, this.idWorld, this.idCatalogue).then(response => {
        SwalComponent.toast({
          type: 'success',
          title: 'GA agregado al catálogo'
        });
        this.loadIcon = "delete_forever";
        this.tooltip = "Eliminar del Catálogo";
        this.exist = 1;
        this.idGaLocal = +response;
      }).catch(e => {
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido agregar el GA al Catálogo, por favor intente nuevamente'
        });
      });;
    }


  }
  //Snackbar que aparece cuando clickean un GA sin haberlo agregado
  openSnackBar() {
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: 3000,
    });
  }

  setExistCard() {
    let classes = {
      'show-red': this.exist
    };
    return classes;
  }

  //Abre el modal para eliminar estructuras de gas
  openModalDeleteGaStructure(id): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: {component: DeleteGaStructureComponent, title: "Quitar GA del Catálogo", id:id}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == 'deleted') {
        //Quita el color rojo
        this.exist = 0;
        this.tooltip = "Añadir al Catálogo";
        this.loadIcon = "archive";
      }
    });
  }

}
