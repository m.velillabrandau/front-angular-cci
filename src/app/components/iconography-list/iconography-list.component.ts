import { Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DialogComponent } from '../shared/dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { AddIconographyComponent } from './add-iconography/add-iconography.component';
import { IconographyService } from '../../services/iconography.service';
import { SwalComponent } from '../../components/shared/swal/swal.component';
import { EditIconographyComponent } from './edit-iconography/edit-iconography.component';
import { DeleteIconographyComponent } from './delete-iconography/delete-iconography.component';
import { DataSource } from '@angular/cdk/table';
import { environment } from '../../../environments/environment';

export interface IconographyData {
	titulo: string;
  img_url: string;
}
@Component({
  selector: 'app-iconography-list',
  templateUrl: './iconography-list.component.html',
  styleUrls: ['./iconography-list.component.css']
})

export class IconographyListComponent implements OnInit {
  displayedColumns: string[] = ['imagen','titulo', 'img_url','acciones'];
  dataSource: MatTableDataSource<IconographyData>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataIconography = {
    id: null,
    title: null,
    url: null,
    path: null
  };

  ruteTags = environment.URLTags; //rute tags

  constructor(public dialog: MatDialog, private iconographyService: IconographyService) { }

  ngOnInit() {
    this.getIconographies();
    this.iconographyService.change.subscribe(change => {
      this.getIconographies();
    });
  }

  //Obtiene conjunto de iconos
  getIconographies(){
    this.iconographyService.getIconographies().subscribe(
			response=>{
				if(response!=''&&response!=undefined){

          this.dataSource = new MatTableDataSource(response as Array<IconographyData>);
          //Configuraciones paginador y ordenado
          this.paginator._intl.itemsPerPageLabel = 'Registros por página';
          this.paginator._intl.nextPageLabel = 'Página siguiente';
          this.paginator._intl.previousPageLabel = 'Página anterior';
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          return true;

				}else{
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido traer los Tags'
          });
					return false;
				}
			},error=>{
				console.log(error);
				return false;
			}
		);
  }

  //Obtiene un icono especifico
  getIconography(id){
    this.iconographyService.getIconography(id).subscribe(
			response=>{
        this.dataIconography.id = id;
        this.dataIconography.title = response.titulo;
        this.dataIconography.url= response.img_url;
        this.dataIconography.path= response.img_path;

				if(response!=''&&response!=undefined){
          const dialogRef = this.dialog.open(DialogComponent, {
            width: '500px',
            height: '255px',
            data: { component: EditIconographyComponent, title: "Editar Ícono", data: this.dataIconography }
          });

          // return response;

				}else{
					// return response;
				}
			},error=>{
				console.log(error);
				// return false;
			}
		);
  }

  applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
  }

  //Funcion para abrir Modal para agregar icono
  openModalIconography(){
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '500px',
      height: '255px',
      data: {component: AddIconographyComponent, title: "Agregar Iconografía"}
    });
  }

   //Funcion para abrir Modal para editar un icono
   openModalEditIconography(id){
     this.getIconography(id);
  }

  //Funcion para abrir Modal para eliminar icono
  openModalDeleteIconography(id): void{
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '340px',
      data: { component: DeleteIconographyComponent, title: "Eliminar Iconografía", id: id }
    });
  }
}
