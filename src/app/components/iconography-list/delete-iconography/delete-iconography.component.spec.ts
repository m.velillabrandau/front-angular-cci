import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteIconographyComponent } from './delete-iconography.component';

describe('DeleteIconographyComponent', () => {
  let component: DeleteIconographyComponent;
  let fixture: ComponentFixture<DeleteIconographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteIconographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteIconographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
