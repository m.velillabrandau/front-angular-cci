import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from "../../shared/dialog/dialog.component";
import { IconographyService } from '../../../services/iconography.service';
import { SwalComponent } from '../../shared/swal/swal.component';

@Component({
  selector: 'app-delete-iconography',
  templateUrl: './delete-iconography.component.html',
  styleUrls: ['./delete-iconography.component.css']
})
export class DeleteIconographyComponent implements OnInit {

  data: Array<any> = [];

  constructor(public dialogRef: MatDialog, public dialog: DialogComponent, public iconographyService: IconographyService) { }

  ngOnInit() {
    this.data = this.dialog.getData();
  }

  onNoClick(): void {
    this.dialogRef.closeAll();
  }

  deleteIconography() {
    this.iconographyService.deleteIconography(this.data["id"]);
    this.dialogRef.closeAll();
  }

}
