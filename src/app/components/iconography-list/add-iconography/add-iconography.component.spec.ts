import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIconographyComponent } from './add-iconography.component';

describe('AddIconographyComponent', () => {
  let component: AddIconographyComponent;
  let fixture: ComponentFixture<AddIconographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIconographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIconographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
