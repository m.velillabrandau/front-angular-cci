import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SwalComponent } from '../../shared/swal/swal.component';
import { IconographyService } from '../../../services/iconography.service';

@Component({
  selector: 'app-add-iconography',
  templateUrl: './add-iconography.component.html',
  styleUrls: ['./add-iconography.component.css']
})
export class AddIconographyComponent implements OnInit {

  iconographyAddForm: FormGroup;
  selectedFile: File = null;
  test: string = null;
  base64textString: string;
  eventSelected;

  constructor(public dialogRef: MatDialog, private iconographyService: IconographyService) {

  }

  ngOnInit() {
    this.iconographyAddForm = new FormGroup({
      'title': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'imgfile': new FormControl(null,[
        Validators.required
      ])
    })
  }
   // Cierra popup de ingreso
   onNoClick(): void {
    this.dialogRef.closeAll();
  }

  //Capturando imagen
  onFileSelected(event){

    this.selectedFile = event.target.files[0];
  }

  // Submit
  onSubmit() {
    if (this.iconographyAddForm.valid) {

      const uploadData = new FormData();
      uploadData.append('img_url', this.selectedFile, this.selectedFile.name);
      uploadData.append('titulo', this.title.value);
      this.iconographyService.addIconography(uploadData);

      this.dialogRef.closeAll();
    }
  }

  get title() { return this.iconographyAddForm.get('title'); }

}
