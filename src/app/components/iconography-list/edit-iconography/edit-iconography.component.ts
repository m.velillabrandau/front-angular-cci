import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SwalComponent } from '../../shared/swal/swal.component';
import { IconographyService } from '../../../services/iconography.service';
import { DialogComponent } from "../../shared/dialog/dialog.component";

@Component({
  selector: 'app-edit-iconography',
  templateUrl: './edit-iconography.component.html',
  styleUrls: ['./edit-iconography.component.css']
})
export class EditIconographyComponent implements OnInit {

  data: Array<any> = [];
  iconographyEditForm: FormGroup;
  selectedFileEdit: File = null;
  eventSelected;

  constructor(public dialog: DialogComponent ,public dialogRef: MatDialog, private iconographyService: IconographyService) { }

  ngOnInit() {
    this.data = this.dialog.getData();

    this.iconographyEditForm = new FormGroup({
      'title': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'imgfile': new FormControl(null,[
      ])
    })
  }

   // Cierra popup de ingreso
  onNoClick(): void {
    this.dialogRef.closeAll();
  }

  //Capturando imagen
  onFileSelectedEdit(event){

    this.selectedFileEdit = event.target.files[0];

  }

  // Reinicio componentes
  onSubmit() {
    if (this.iconographyEditForm.valid) {
      const uploadData2 = new FormData();
      uploadData2.append('img_url', this.selectedFileEdit, this.selectedFileEdit.name);
      this.iconographyService.editIconography(uploadData2);

      this.dialogRef.closeAll();
    }
  }

  get title() { return this.iconographyEditForm.get('title'); }

}
