import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIconographyComponent } from './edit-iconography.component';

describe('EditIconographyComponent', () => {
  let component: EditIconographyComponent;
  let fixture: ComponentFixture<EditIconographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIconographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIconographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
