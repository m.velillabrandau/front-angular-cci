import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconographyListComponent } from './iconography-list.component';

describe('IconographyListComponent', () => {
  let component: IconographyListComponent;
  let fixture: ComponentFixture<IconographyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconographyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconographyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
