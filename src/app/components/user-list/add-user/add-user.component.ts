import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ErrorStateMatcher } from '@angular/material/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  userAddForm: FormGroup;

  constructor(public dialogRef: MatDialog, private router: Router, public userService: UserService) { }
  // Iniciación de clase
  ngOnInit() {
    this.userAddForm = new FormGroup({
      'user': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'pass': new FormControl(null,[
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
    })
  }

  // Reinicio de componente
  onSubmit() {
    if (this.userAddForm.valid) {
      this.userService.addUser({"username":this.user.value, "password":this.pass.value});
      this.dialogRef.closeAll();
    }
  }

  // Cierra popup de ingreso
  onNoClick(): void {
    this.dialogRef.closeAll();
  }

  get user() { return this.userAddForm.get('user'); }
  get pass() { return this.userAddForm.get('pass'); }
  matcher = new ErrorStateMatcher();

}
