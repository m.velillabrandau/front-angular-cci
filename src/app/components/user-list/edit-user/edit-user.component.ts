import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ErrorStateMatcher } from '@angular/material/core';
import { UserService } from '../../../services/user.service';
import { DialogComponent } from '../../shared/dialog/dialog.component';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  userEditForm: FormGroup;
  data: Array<any> = [];
  userName: string;
  password: string;

  constructor(public dialog: DialogComponent, public dialogRef: MatDialog, private router: Router, public userService: UserService) { }
  // Iniciación de clase
  ngOnInit() {

    this.data = this.dialog.getData();
    this.userName = this.data['username'];
    

    this.userEditForm = new FormGroup({
      'user': new FormControl(this.data['username'],[
        Validators.minLength(4),
        Validators.maxLength(30)
      ]),
      'pass': new FormControl(null,[
      ]),
    })
    // this.userEditForm.value.user = this.data['username'];
    // console.log("ACA1", this.userEditForm.value.user);
  }

  // Reinicio de componente
  onSubmit() {
    if (this.userEditForm.valid) {
      if(this.pass.value == ''){
        this.userService.editUser({"id":this.data['id'] ,"username":this.user.value,"pass":null});
        this.dialogRef.closeAll();
      }else{
        this.userService.editUser({"id":this.data['id'] ,"username":this.user.value,"pass":this.pass.value});
        this.dialogRef.closeAll();
      }

    }
  }

  // Cierra popup de ingreso
  onNoClick(): void {
    this.dialogRef.closeAll();
  }

  getUser(){
    this.userService.getUser(this.data['id']).subscribe(res =>{
      this.userName = res['username'];

    });
  }



  get user() { return this.userEditForm.get('user'); }
  get pass() { return this.userEditForm.get('pass'); }
  matcher = new ErrorStateMatcher();
}
