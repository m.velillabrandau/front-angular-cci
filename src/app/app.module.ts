import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//Material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatCardModule,MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatProgressBarModule, MatTooltipModule, MatSnackBarModule } from '@angular/material';
import { NavComponent } from './nav/nav.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {PortalModule} from '@angular/cdk/portal';
//Flex layout
import { FlexLayoutModule } from '@angular/flex-layout';
//Sweet alert
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
//Páginas
import { UserListComponent } from './components/user-list/user-list.component';
import { CatalogueListComponent } from './components/catalogue-list/catalogue-list.component';
import { FooterComponent } from './footer/footer.component';
import { AddUserComponent } from './components/user-list/add-user/add-user.component';
import { LoginComponent } from './components/login/login.component';
import { DialogComponent } from './components/shared/dialog/dialog.component';
import { AddCatalogComponent } from './components/catalogue-list/add-catalog/add-catalog.component';
import { SwalComponent } from './components/shared/swal/swal.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ServiceNotFoundComponent } from './service-not-found/service-not-found.component';
import { MainCatalogueComponent } from './components/main-catalogue/main-catalogue.component';
import { ListWorldComponent } from './components/main-catalogue/list-world/list-world.component';
import { GaListComponent } from './components/main-catalogue/ga-list/ga-list.component';
import { GaComponent } from './components/main-catalogue/ga-list/ga/ga.component';
import { WorldComponent } from './components/main-catalogue/list-world/world/world.component';
import { HeadCreationComponent } from './components/main-catalogue/main-grid-products/head-creation/head-creation.component';
import { PdfCreationComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/pdf-creation/pdf-creation.component';
import { SquareStrategyComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/superior-labels/square-strategy/square-strategy.component';
import { SubtitleStrategyComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/superior-labels/subtitle-strategy/subtitle-strategy.component';
import { ProductListComponent } from './components/main-catalogue/product-list/product-list.component';
import { ProductComponent } from './components/main-catalogue/product-list/product/product.component';
import { MainGridProductsComponent } from './components/main-catalogue/main-grid-products/main-grid-products.component';
import { GridComponent } from './components/main-catalogue/main-grid-products/grid/grid.component';
import { GridFooterComponent } from './components/main-catalogue/main-grid-products/grid-footer/grid-footer.component';
import { SuperiorLabelsComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/superior-labels/superior-labels.component';
import { SquareGridComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/square-grid.component';
// Dragula
import { DragulaModule } from 'ng2-dragula';
import { HttpClientModule } from '@angular/common/http';
import { DeleteCatalogueComponent } from './components/catalogue-list/delete-catalogue/delete-catalogue.component';

import {Globals} from './globals'
import { DeleteUserComponent } from './components/user-list/delete-user/delete-user.component';
import { IconographyListComponent } from './components/iconography-list/iconography-list.component';
import { AddIconographyComponent } from './components/iconography-list/add-iconography/add-iconography.component';
import { DeleteIconographyComponent } from './components/iconography-list/delete-iconography/delete-iconography.component';
// import { EditCatalogueComponent } from './components/catalogue-list/edit-catalogue/edit-catalogue.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptorService } from './services/Loader-interceptor.service';
import { ProgressBarComponent } from './nav/progress-bar/progress-bar.component';
import { EditCatalogComponent } from './components/catalogue-list/edit-catalog/edit-catalog.component';
import { DeleteWorldStructureComponent } from './components/main-catalogue/list-world/world/delete-world-structure/delete-world-structure.component';
import { SnackBarComponent } from './components/main-catalogue/ga-list/ga/snack-bar/snack-bar.component';
import { WorldSnackBarComponent } from './components/main-catalogue/list-world/world/world-snack-bar/world-snack-bar.component';
import { LogoutComponent } from './components/logout/logout.component';
import { EditIconographyComponent } from './components/iconography-list/edit-iconography/edit-iconography.component';
import { DeleteGaStructureComponent } from './components/main-catalogue/ga-list/ga/delete-ga-structure/delete-ga-structure.component';
import { CombineModalComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/pdf-creation/combine-modal/combine-modal.component';
import { OptionalTextComponent } from './components/main-catalogue/main-grid-products/grid/square-grid/pdf-creation/optional-text/optional-text.component';
import { EditUserComponent } from './components/user-list/edit-user/edit-user.component';
import { SkumodPipe } from './pipes/skumod.pipe';
import { NoblankDirective } from './directives/noblank.directive';

@NgModule({
	declarations: [
		AppComponent,
		NavComponent,
		UserListComponent,
		CatalogueListComponent,
		FooterComponent,
    AddUserComponent,
    LoginComponent,
    DialogComponent,
    AddCatalogComponent,
    SwalComponent,
    PageNotFoundComponent,
    ServiceNotFoundComponent,
    MainCatalogueComponent,
    ListWorldComponent,
    GaListComponent,
    GaComponent,
    WorldComponent,
    HeadCreationComponent,
    PdfCreationComponent,
    SquareStrategyComponent,
    SubtitleStrategyComponent,
    ProductListComponent,
    ProductComponent,
    MainGridProductsComponent,
    GridComponent,
    GridFooterComponent,
    SuperiorLabelsComponent,
    SquareGridComponent,
    DeleteCatalogueComponent,
    DeleteUserComponent,
    IconographyListComponent,
    AddIconographyComponent,
    DeleteIconographyComponent,
    AddIconographyComponent,
    ProgressBarComponent,
    DeleteWorldStructureComponent,
    SnackBarComponent,
    WorldSnackBarComponent,
    LogoutComponent,
    DeleteGaStructureComponent,
    EditIconographyComponent,
    EditCatalogComponent,
    WorldSnackBarComponent,
    CombineModalComponent,
    OptionalTextComponent,
    EditUserComponent,
    SkumodPipe,
    NoblankDirective
  ],
  entryComponents: [OptionalTextComponent, CombineModalComponent, AddIconographyComponent, DialogComponent, AddCatalogComponent, AddUserComponent, DeleteCatalogueComponent, DeleteUserComponent, DeleteIconographyComponent, EditCatalogComponent, DeleteWorldStructureComponent, EditIconographyComponent, WorldSnackBarComponent, SnackBarComponent, DeleteGaStructureComponent, EditUserComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MatTabsModule,
		MatCardModule,
		DragDropModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatGridListModule,
		MatMenuModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		FlexLayoutModule,
		MatFormFieldModule,
		MatInputModule,
		MatChipsModule,
    MatDialogModule,
    SweetAlert2Module,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
    MatSelectModule,
    MatCheckboxModule,
    DragulaModule.forRoot(),
    HttpClientModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatSnackBarModule
	],
    providers: [Globals,
            {provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptorService,
            multi: true},
            GridFooterComponent
    ],
  	bootstrap: [AppComponent]
})
export class AppModule { }
