import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'skumod'
})
export class SkumodPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.substring(5,6) + ' ' + value.substring(6,12) + ' ' + value.substring(12,15) + ' ' + value.substring(15,18);
  }

}
