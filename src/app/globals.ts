import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
@Injectable()
//Información Global para el sistemas
export class Globals {
  // Ruta en la cual se encuentran los recursos de los microservicios
  // rute: string = 'https://my-json-server.typicode.com/Adrain6/demo';
  // rute: string = 'http://192.168.10.151/Rest-CI'

  // Manejador de errores para los servicios
  public handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // Error
      console.log(`${operation} failed: ${error.message}`);
      // Se retorna un resultado vacío para no cortar el proceso de la app
      return of(result as T);
    };
  }
}
