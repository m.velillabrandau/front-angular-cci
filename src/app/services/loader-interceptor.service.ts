import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from "@angular/router"
import { LoaderService } from './loader.service';



const jwtHelper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class LoaderInterceptorService implements HttpInterceptor {

  constructor(private loaderService: LoaderService, private router:Router) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.showLoader();
    const idToken = sessionStorage.getItem("id_token");
    if (idToken && !req.url.includes("api/users/token")) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization",
          "Bearer " + idToken)
      });
      return next.handle(cloned).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.onEnd();
        }
      },
        (err: any) => {
          console.log('..cloned error: ',err);
          this.onEnd();
          throwError(err);
          //todo: cambiar a refrescar el token cuando expira y no hacer el redirect
          //this.router.navigate(['/logout'])

          /* this.onEnd();
          throwError(err); */
        }));
    } else {
      return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.onEnd();
        }
      },
        (err: any) => {
          console.log('..cloned error: ', err);
          this.onEnd();
          if(err.status == 401){
            console.log('..sesión finalizada');
            this.router.navigate(['/logout/1']);
            
          }
          //throwError(err);
          //todo: cambiar a refrescar el token cuando expira y no hacer el redirect
          //this.router.navigate(['/logout'])

        /* this.onEnd();
        throwError(err); */
        }));
    }
  }
  private onEnd(): void {
    this.hideLoader();
  }
  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }
}
