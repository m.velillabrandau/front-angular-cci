import { TestBed } from '@angular/core/testing';

import { FeriaService } from './feria.service';

describe('FeriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeriaService = TestBed.get(FeriaService);
    expect(service).toBeTruthy();
  });
});
