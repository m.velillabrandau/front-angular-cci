import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';
import { SwalComponent } from './../components/shared/swal/swal.component';
const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient, private globals: Globals) { }
  public getProducts(idGa): Observable<any> {
    return this.http.get(APIEndpoint + '/contents?analysis_groups_id='+idGa);
  }
  // Guarda un producto en una grilla
  public saveContent(content) {

    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/contents', content).toPromise().then(res=> {
        resolve(res); // Promesa exitosa respuesta id ga
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }

  //Guarda uno icono en producto
  public saveIconographyProduct(sku_product, data){
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/contents/saveiconographyproduct/' + sku_product, data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Tag agregado al producto'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }
  removeContent(id){
    let promise = new Promise((resolve, reject) => {
      this.http.delete<any>(APIEndpoint + '/contents/' + id).toPromise().then(res=> {
       resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      });
    });
    return promise;
}
  //Guarda los valores del producto (Menos, Mas y Nuevo)
  public setValueProduct(sku_product, data){
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/contents/setvalueproduct/' + sku_product, data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Valoración agregada al producto'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  //Guarda todos los valores del producto padre he hijos (Menos, Mas y Nuevo)
  public setValueAllProduct(idContent, data){
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/contents/setvalueallproduct/' + idContent, data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Valoración agregada al producto'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  //Guarda el texto opcional de un producto
  public saveOptionalTextProduct(sku_product, data){
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/contents/saveoptionaltext/' + sku_product, data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Texto agregado al producto'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  //Verifica si existe un producto en la grilla
  public checkProduct (idPage, corder): Observable<any> {
      return this.http.get(APIEndpoint + '/ags/checkexistproduct/'+idPage+'/'+corder);
  }

  //Obtiene los contenidos por un id de página
  public getContentsFrompage(id) {
    let promise = new Promise((resolve, reject) => {
      this.http.get(APIEndpoint + '/ags/getcontentsfrompage/'+id).toPromise().then(res => {
        resolve(res);
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      })
    });
    return promise;
  }
  public combine(body) {
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/contents/combine', body).toPromise().then(res=> {
        resolve(res); // Promesa exitosa
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }
}
