import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';
import { SwalComponent } from './../components/shared/swal/swal.component';
import { MediatorService } from './mediator.service';
import { catchError } from 'rxjs/operators';
import {HttpParams} from  "@angular/common/http";
const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})
export class GaService {

  @Output() change: EventEmitter<any> = new EventEmitter(); // Evento cambio mundo para los ga

  constructor(private mediator: MediatorService, private http: HttpClient, private globals: Globals) { }
  // Emite los gas
  toggleWorld(gas, idWorld, idCatalogue, idWorldLocal) {
    for (var key in gas) {
      if (gas.hasOwnProperty(key)) {
        gas[key]['idWorld'] = idWorld;
        gas[key]['idCatalogue'] = idCatalogue;
        gas[key]['idWorldLocal'] = idWorldLocal;
      }
    }
    this.change.emit(gas);
  }
  // Carga un determinado ga en la base de datos local
  loadGa(id, name, idWorld, idCatalogue) {
    let body = {
      id: id,
      name: name,
      idWorld: idWorld,
      idCatalogue: idCatalogue
    }
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/ags/savelocalga', body, httpOptions).toPromise().then(res=> {
        this.mediator.reorderGa();
        resolve(res); // Promesa exitosa
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }
  // Carga los GAs que están guardados en la base de datos local
  loadLocalGas(idLocalWorld) {
    let promise = new Promise((resolve, reject) => {
      this.http.get(APIEndpoint+'/ags/getlocalga/'+idLocalWorld, httpOptions).toPromise().then(data => {
        resolve(data);
      });
    });
    return promise;
  }
  //Elimina un ga local del catálogo
  removeGa(id){
      let promise = new Promise((resolve, reject) => {
        this.http.delete<any>(APIEndpoint + '/ags/' + id).toPromise().then(res=> {
         resolve();
        },
        msg => {
          console.log(msg); //error ocurrido
          reject(); //Promesa fallida
        });
      });
      return promise;
  }
  //obtiene la primera página de un GA
  getFirstPage(idLocalGa) {
    let promise = new Promise((resolve, reject) => {
      this.http.get<any>(APIEndpoint + '/ags/getfirstpage/' + idLocalGa).toPromise().then(res=> {
        resolve(res);
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
       });
    });
    return promise;
  }
  // Set a las dimensiones de una página
  setDimentions(x, y, page, idCat) {
    let body = {
      x: x,
      y: y,
      id_cat: idCat
    }
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/ags/resizepage/'+page, body, httpOptions).toPromise().then(res=> {
        resolve(res); // Promesa exitosa respuesta id ga
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }

   //obtiene la primera página de un GA
   addIconFooterPage(idLocalGa, data) {
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/ags/addiconfooterpage/' + idLocalGa, data, httpOptions).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Pie de página agregado'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  //Listado de iconos
  public getIconPage(idPage): Observable<any>{
    return this.http.get<any>(APIEndpoint + '/ags/geticonpage/' + idPage).pipe(
      catchError(this.globals.handleError<any>('getIconographies'))
    );
  }
  public orderGas(content) {
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/ags/reorder', content).toPromise().then(res=> {
        resolve(res); // Promesa exitosa
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }

  addPage(data){
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/pages/',data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Nueva página agregada'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  deletePage(data){
    let promise = new Promise((resolve, reject) => {
      this.http.delete<any>(APIEndpoint + '/pages/'+data.id_page).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Página eliminada'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  getPages(idPage): Observable<any>{
    return this.http.get<any>(APIEndpoint + '/pages/getpages/' + idPage).pipe(
      catchError(this.globals.handleError<any>('getPages'))
    );
  }

  getPage(idPage): Observable<any>{
    return this.http.get<any>(APIEndpoint + '/pages/getpagewithproduct/' + idPage).pipe(
      catchError(this.globals.handleError<any>('getPages'))
    );
  }
  getPageP(idPage){
    let promise = new Promise((resolve, reject) => {
      this.http.get(APIEndpoint + '/pages/getpagewithproduct/' + idPage).toPromise().then(res => {
        resolve(res);
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      })
    });
    return promise;
  }

  addSquareStrategy(data){
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/pages/addsquarestrategy/', data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Cuadro de estrategia agregado'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  addSubtitleStrategy(data){
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/pages/addsubtitlestrategy/', data).toPromise().then(res=> {
        resolve(res);
        SwalComponent.toast({
          type: 'success',
          title: 'Subtítulo de estrategia agregado'
        });
       },
       msg => {
         console.log(msg); //error ocurrido
         reject(); //Promesa fallida
         SwalComponent.toast({
          type: 'error',
          title: 'Ha ocurrido un error al procesar la solicitud'
        });
       });
    });
    return promise;
  }

  //Trae los contents groups de una pagina especifica
  public getContentGroupByPage(id){

    let promise = new Promise((resolve, reject) => {
      const  params = new  HttpParams().set('idPage', id);
      this.http.get(APIEndpoint + '/ags/getcontentgroupbypage/', {params}).toPromise().then(res => {
        resolve(res);
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      })
    });
    return promise;


  }
}
