import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';
import { SwalComponent } from './../components/shared/swal/swal.component';
import { catchError } from 'rxjs/operators';
import { MediatorService } from './mediator.service';

const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})

export class WorldService {
  @Output() change: EventEmitter<boolean> = new EventEmitter(); // Evento cambio mundo para los ga
  constructor(private http: HttpClient, private globals: Globals, private mediator: MediatorService) { }
  // Obtiene todos los mundos
  public getWorlds(idMagazine, idFeria): Observable<any> {
    //Por ahora, se retornan los mundos traidos por un id de feria desde corporativo
    return this.http.get(APIEndpoint+'/magazines/'+idMagazine+'/worlds/'+'getworlds/'+idFeria);
  }
  // Obtiene todos los mundos guardados en la base de datos local a través de un id magazine
  public getWorldsLocal(idMagazine): Observable<any> {
    //Por ahora, se retornan los mundos traidos por un id de feria desde corporativo
    return this.http.get(APIEndpoint+'/worlds/getlocalworlds/'+idMagazine);
  }
  //Elimina de la base local el mundo y los gas correspondientes
  public deleteStructure(idcat, idworld) {
    let body = {
      idCatalogue: idcat,
      idworld: idworld
    }
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/worlds/deleteWorldStructure', body, httpOptions).toPromise().then(res=> {
        SwalComponent.toast({
          type: 'success',
          title: 'Mundo eliminado del catálogo'
        });
        resolve(); //Promesa exitosa
      },
      msg => {
        console.log(msg); //error ocurrido
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido eliminar el mundo del catálogo, por favor intente nuevamente'
        });
         //Promesa fallida
         reject('error');
      });
    });
    return promise;
  }
  //Crea la estructura del catalogo en la base de datos local, carga mundos
  public createStructure(data) {
    let body = {
      data: JSON.stringify(data)
    }
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/worlds/saveworldstructure', body, httpOptions).toPromise().then(res=> {
        SwalComponent.toast({
          type: 'success',
          title: 'Mundo agregado al catálogo'
        });
        this.mediator.reorderWorlds();
        resolve(res); //Promesa exitosa
      },
      msg => {
        console.log(msg); //error ocurrido
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido agregar el mundo al catálogo, por favor intente nuevamente'
        });
        reject(); //Promesa fallida
      });
    });
    return promise;
  }
  //Obtiene un mundo local en especifico
  public getLocalWorld(idWorldLocal): Observable<any> {
    return this.http.get(APIEndpoint+'/worlds/'+idWorldLocal).pipe(
      catchError(this.globals.handleError<any>('getLocalWorld'))
    );
  }

  public editWorld (id, data) {
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/worlds/editcover/'+id, data).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Se ha editado la portada'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido editar, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
  /**
   * Actualiza el alias del mundo si este es modificado en el editor
   */
  public setWorldAlias(data){
    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/worlds/changeAlias/', data).toPromise().then(res => {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Mundo editado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido editar el mundo, por favor intente nuevamente'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
  // Cambia el orden de los productos
  public orderProducts(world) {
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/worlds/reorder', world).toPromise().then(res=> {
        resolve(res); // Promesa exitosa
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      });
    });
    return promise;
  }
}
