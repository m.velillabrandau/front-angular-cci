import { Injectable, Output, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Globals } from '../globals'
import { environment } from './../../environments/environment';
import { SwalComponent } from './../components/shared/swal/swal.component';

const APIEndpoint = environment.APIEndpoint; //rute api
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};
@Injectable({
  providedIn: 'root'
})
export class IconographyService {

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: HttpClient, private globals: Globals) { }

  //Listado de iconos
  public getIconographies(): Observable<any>{
    return this.http.get<any>(APIEndpoint + '/tags').pipe(
      catchError(this.globals.handleError<any>('getIconographies'))
    );
  }

  public getIconographies2(){
    let promise = new Promise((resolve, reject) => {
      this.http.get(APIEndpoint + '/tags').toPromise().then(res => {
        resolve(res);
      },
      msg => {
        console.log(msg); // error ocurrido
        reject(); // Promesa fallida
      })
    });
    return promise;
  }

  //Trae un icono especifico
  public getIconography(id): Observable<any>{
    return this.http.get<any>(APIEndpoint + '/tags/'+id).pipe(
      catchError(this.globals.handleError<any>('getIconography'))
    );
  }
  // Agrega icono
  public addIconography (data){

    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(APIEndpoint + '/tags', data).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Ícono ingresado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido guardar el ícono.'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
  // Edita un icono
  public editIconography (data){

    let promise = new Promise((resolve, reject) => {
      this.http.put<any>(APIEndpoint + '/tags/51', data, httpOptions).toPromise().then(res=> {
        this.change.emit(true); //Se emite el evento de que se ha guardado algo, este evento debe de ser suscrito
        SwalComponent.toast({
          type: 'success',
          title: 'Ícono editado'
        });
        resolve(); //Promesa exitosa
      },
        msg => {
          console.log(msg); //error ocurrido
          SwalComponent.toast({
            type: 'error',
            title: 'No se ha podido editar el ícono.'
          });
          reject(); //Promesa fallida
        }
      )
    });
    return promise;
  }
  // Elimina icono
  public deleteIconography (id){
    let promise = new Promise((resolve, reject) => {
      this.http.delete<any>(APIEndpoint + '/tags/' + id).toPromise().then(res=> {
        this.change.emit(true);
        SwalComponent.toast({
          type: 'success',
          title: 'Ícono eliminado'
        });
      },
      msg => {
        console.log(msg); //error ocurrido
        SwalComponent.toast({
          type: 'error',
          title: 'No se ha podido eliminar el ícono, por favor intente nuevamente'
        });
        reject(); //Promesa fallida
      }
      );
    });
  }

  //Agrega icono al pie de pagina

}
