import { TestBed } from '@angular/core/testing';

import { IconographyService } from './iconography.service';

describe('IconographyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IconographyService = TestBed.get(IconographyService);
    expect(service).toBeTruthy();
  });
});
